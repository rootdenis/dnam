---
layout: post
title: Проверка времени ответа web сервера
date: 2016-06-01 08:48:00 +0300
slug: curl
categories: ru net
locale: ru_RU
lang: ru
Summary: Проверка времени ответа web сервера с помощью curl
---

**Пример вызова**

```bash
curl -s -w '\nLookup time:\t%{time_namelookup}\nConnect time:\t%{time_connect}\nAppCon time:\t%{time_appconnect}\nRedirect time:\t%{time_redirect}\nPreXfer time:\t%{time_pretransfer}\nStartXfer time:\t%{time_starttransfer}\n\nTotal time:\t%{time_total}\n' -o /dev/null https://dnam.su
```


| ключи                | параметры                                                                                               |
|----------------------|---------------------------------------------------------------------------------------------------------|
| `-s, --silent`       | Тихий режим, не показывать индикатора выполнения или сообщений об ошибках                               |
| `-o, output <file>`  | Перенаправляет вывод в ...                                                                              |
| `-I, --head`         | Показать только HTTP заголовки                                                                          |
| `-f, --fail`         | Не сообщать об ошибках на сервере                                                                       |
| `-S, --show-error`   | Вместе с -s, показывает ошибку, в случае когда curl завершается с ошибкой                               |
| `-L, --location`     | При ответе с кодом 3ХХ, заставляет curl выполнять запрос по новому адресу                               |
| `-w, write-out`      | Определяет, что отображается после завершенной и успешной операции                                      |
| `time_total`         | Общее время                                                                                             |
| `time_namelookup`    | Lookup time время резольвинга домена в IP                                                               |
| `time_connect`       | Connect time подключение к удаленному серверу по TCP                                                    |
| `time_pretransfer`   | PreXfer time подготовка к обмену данными., состоит из 'обмен рукопожатиями'                             |
| `time_starttransfer` | StartXfer time затрачено всего до начала передачи., состотит из 'time_pretransfer', подсчет результатов |
| `time_appconnect`    | AppCon time с начала замера, до завершения соединения/рукопожатия                                       |
| `time_redirect`      | Redirect time все редиректы, состоит из name lookup, connect, pretransfer и transfer                    |
| `http_code`          | Код возврата сервера                                                                                    |



**Использование некоторых опций в скриптиах**

```bash
curl -fsSL https://get.docker.com/ | sh
```

###### Ссылки #

* [cURL](https://curl.haxx.se/){:rel="nofollow" target="blank"}
* [cURL docs](https://curl.haxx.se/docs/manpage.html){:rel="nofollow" target="_blank"}
