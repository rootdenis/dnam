---
layout: post
curlang: ru
title: Примеры настройка Nginx прокси
date: 2016-05-20 12:38
slug: nginx
categories: ru net web
locale: ru_RU
lang: ru
summary: Настройка прокси nginx + настройка балансировщика нагрузки
---

#### NGINX - 301 Редирект Moved Permanently #

301 Moved Permanently, редирект, говорящий что ресурс перемещен на постоянной основе. В интернетах пишут что мол типа архинеобходимо для SEO, мол поисковики это дюже уважают), спорить не буду, не вникал. В веб сервере Nginx 301 редирект настраивается в конфигурационном файле ( в apache можно через файл .htaccess ), таким образом:

**1 вариант**
```bash

server {
     listen 80;
     listen [::]:80;
     server_name  www.dtulyakov.ru;
     location / { return 301 http://dtulyakov.ru$request_uri; }
}

server {
     listen 80;
     listen [::]:80;
     server_name dtulyakov.ru;
}
```
**2 вариант - как нельзя делать**
```bash

server {
    server_name dtulyakov.ru;

    if ( $host = 'www.dtulyakov.ru' ) {
        rewrite  ^/(.*)$  http://dtulyakov.ru/$1  permanent;
}
```
#### Балансировщик #

**проксирование простой вариант**
```bash

upstream j_server {
  server 10.0.3.100:8080 fail_timeout=0;
}

server {
  listen 80;
  listen [::]:80;
  server_name jenkins.dtulyakov.ru;
  reset_timedout_connection on;
  charset UTF-8;
  location / {
    proxy_pass http://j_server;
    proxy_next_upstream error timeout invalid_header http_500 http_503;
    #proxy_set_header Host $host;
    proxy_set_header Host $http_host;
    proxy_set_header X-Real-IP $remote_addr;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_redirect off;
    proxy_connect_timeout 30;
  }
}
```

**проксирование вариант с балансировкой**
```bash

upstream j_server {
  server 10.0.3.100:8080 weight=10;
  server 10.0.3.101:8080 max_fails=3 fail_timeout=30s;
  server 10.0.3.102:8080 max_fails=2;
  server 10.0.3.103:8080 backup fail_timeout=0;
}
```

**проксирование вариант с балансировкой**
```bash

upstream j_server {
  least_conn;
  server 10.0.3.100:8080 weight=10;
  server 10.0.3.101:8080 max_fails=3 fail_timeout=30s;
  server 10.0.3.102:8080 max_fails=2;
  server 10.0.3.103:8080 backup fail_timeout=0;
}
```

**проксирование вариант с балансировкой Hash и IP hash**
Для каждого запроса Nginx вычисляет хэш, который состоит из текста, переменных веб-сервера или их комбинации, а затем сопоставляет его с бэкендами
Для вычисления хэша используется схема (http или https) и полный URL
```bash

upstream j_server {
  hash $scheme$request_uri;
  server 10.0.3.100:8080 weight=10;
  server 10.0.3.101:8080 max_fails=3 fail_timeout=30s;
  server 10.0.3.102:8080 max_fails=2;
  server 10.0.3.103:8080 backup fail_timeout=0;
}
```

IP hash работает только с HTTP, это предопределенный вариант, в котором хэш вычисляется по IP-адресу клиента
Если адрес клиента IPv4, то для хэша используется первые три октета, если IPv6, то весь адрес
```bash

upstream j_server {
  ip_hash;
  server 10.0.3.100:8080 weight=10;
  server 10.0.3.101:8080 max_fails=3 fail_timeout=30s;
  server 10.0.3.102:8080 max_fails=2;
  server 10.0.3.103:8080 backup fail_timeout=0;
}
```

* `max_fails` - задает количество неудачных попыток подключений, после которых бэкенд определенное время считается недоступным
* `fail_timeout` - время, в течение которого сервер считается недоступным
* `round-robin` - Веб-сервер по умолчанию распределяет запросы равномерно между бэкендами (но с учетом весов). Этот стандартный метод в Nginx, так что директива включения отсутствует
* `least_conn` - Запросы сначала отправляются бэкенду с наименьшим количеством активных подключений (но с учетом весов)  Если количество активных соединений одинаково, то дополнительно используется метод round-robin
* `max_fails` - задает количество неудачных попыток подключений, после которых бэкенд определенное время считается недоступным
* `fail_timeout` - время, в течение которого сервер считается недоступным
