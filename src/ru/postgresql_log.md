---
layout: post
title:  "Как включить лог запросов в PostgreSQL"
date:   2017-02-16 07:31:10 +0300
slug: postgres_log
categories: ru PoatqreSQL SQL
locale: ru_RU
lang: ru
summary: "Как включить логирование всех запросов в PostgreSQL"
---

##### Как включить логирование в PostgreSQL

<img src="/images/postgresql/PostgreSQL.svg" style="width: 100.0px; height: 100.0px;">

Для начала надо в конфиге указать

```text
#log_directory = 'pg_log' to log_directory = 'pg_log'
#log_filename = 'postgresql-%Y-%m-%d_%H%M%S.log' to log_filename = 'postgresql-%Y-%m-%d_%H%M%S.log'
#log_statement = 'none' to log_statement = 'all'
#logging_collector = off to logging_collector = on
```

Это потом можно будет делать на лету если выставить значение в `false`


```sql
SELECT
  set_config(
    'log_statement',
    'all',
    true
  );
```

Перезагружаем демон

{% highlight bash %}
systemctl restart postgresql
```

Делаем запрос

```sql
select 2+2
```

Искать логи надо в /var/lib/pgsql/9.6/data/pg_log/

