---
layout: post
title:  "Туннель"
date:   2015-08-02 10:02:00 +0300
slug: tunnel
categories: ru net
locale: ru_RU
lang: ru
summary: "Туннель между FreeBSD и Linux"
---

##### Вводные данные для примера

```bash

Server1:
ОС: Linux
Сетевой интерфейс: eth0
IP: 100.100.100.100
Server2:
ОС: FreeBSD
Сетевой интерфейс: fxp0
IP: 200.200.200.200
```

Нам необходимо получить IPv4 over IPv4 тунель со следующими параметрами между указанными серверами:

```bash

Server1: 10.0.0.1 / 255.255.255.252
Server2: 10.0.0.2 / 255.255.255.252
```

##### Настройка Linux
Для настройки описанной конфигурации на Linux-сервере нам нужно выполнить следующие шаги:

Создадим ipip-тунельный интерфейс:

```bash

ip tunnel add tun0 mode ipip remote 200.200.200.200 local 100.100.100.100 dev eth0
```

Установим IP-адреса:

```bash

ifconfig tun0 10.0.0.1 netmask 255.255.255.252 pointopoint 10.0.0.2
```

Установим MTU и поднимем интерфейс:

```bash

 ifconfig tun0 mtu 1500 up
```

Теперь на Linux-сервере мы имеем следующий интерфейс:

```bash

ifconfig tun0 tun77 Link encap:IPIP Tunnel HWaddr

inet addr:10.0.0.1  P-t-P:10.0.0.2  Mask:255.255.255.252
UP POINTOPOINT RUNNING NOARP  MTU:1500  Metric:1
RX packets:6 errors:0 dropped:0 overruns:0 frame:0
TX packets:6 errors:0 dropped:0 overruns:0 carrier:0
collisions:0 txqueuelen:0
RX bytes:504 (504.0 b)  TX bytes:624 (624.0 b)
```

##### Настройка FreeBSD
Следующим шагом будет настройка конца тунеля на стороне FeeeBSD-сервера:

Создаем gif-интерфейс для тунеля:

```bash

ifconfig gif0 create
```

Устанавливаем транспортные IP-адреса:

```bash

ifconfig gif0 tunnel 200.200.200.200 100.100.100.100
ifconfig gif0 inet6 alias MY_IPV6_IP REMOTE_IPV6_IP
```

Устанавливаем IP-адреса тунеля:

```bash

ifconfig gif0 10.0.0.2 netmask 255.255.255.252 10.0.0.1
```

Устанавливаем MTU и поднимаем интерфейс:

```bash

ifconfig gif0 mtu 1500 up
```

В результате на стороне FreeBSD видим следующее:

```bash

ifconfig gif0 gif0: flags=8051 mtu 1500
```

```bash

tunnel inet 200.200.200.200 --> 100.100.100.100
inet 10.0.0.2 --> 10.0.0.1 netmask 0xfffffffc
```
