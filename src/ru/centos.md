---
layout: post
title:  "CentOS"
date:   2019-11-06 00:37:00 +0300
slug: centos
categories: ru OS
locale: ru_RU
lang: ru
summary: "Настройки системных параметров в CentOS"
---

### Proxy

#### Системные настройки

Производится путем добавления настроек в файл `/etc/profile`, строки можно добавить в конец файла:

```
MY_PROXY_URL="http://ServerProxy:8080/"
HTTP_PROXY=$MY_PROXY_URL
HTTPS_PROXY=$MY_PROXY_URL
FTP_PROXY=$MY_PROXY_URL
http_proxy=$MY_PROXY_URL
https_proxy=$MY_PROXY_URL
ftp_proxy=$MY_PROXY_URL
export HTTP_PROXY HTTPS_PROXY FTP_PROXY http_proxy https_proxy ftp_proxy
```

Если необходимо настроить для конкртеного пользователя, то необходимо изменить файл `.bash_profile` в домашней дирректории пользователя, добавив в него (в конец файла) выше приведенные строки. Настройки применяем выходом/входом в систему, в моем случае все решилось использованием команды:
```bash
$ source ~/.bash_profile
```

Если необходимо указать имя пользователя и пароль, ту они указываются в таком формате:
```
MY_PROXY_URL="http://user:password@ServerProxy:8080/"
```

#### Yum

Для этого достаточно указать в `/etc/yum.conf` в разделе main данные прокси и данные для аутентификации:
```
[main]
proxy=http://xxx.xxx.xx.x:8080
proxy_username=domain\user
proxy_password=password
```

### Network

Файлы в каталоге `/etc/sysconfig/network-scripts` с именами вида `ifcfg-eth0`.

#### DHCP
```
# динамическая конфигурация интерфейса
ONBOOT="yes"
DEVICE="eth0"
BOOTPROTO="dhcp"
```

#### Static address
```
# статическая конфигурация интерфейса
ONBOOT="yes"
DEVICE="eth1"
BOOTPROTO="static"
IPADDR=145.14.137.221
PREFIX=24
# конфигурация шлюза и DNS
GATEWAY=192.168.1.1
DEFROUTE=yes
DNS1=192.168.1.14
# NM - NetworkManager - графическая оболочка + автоматизация настроек
NM_CONTROLLED="no"
```

#### Дополнительный IP адрес на интерфейсе eth1
```
ONBOOT=no
DEVICE=eth1:0
BOOTPROTO=static
IPADDR=172.16.12.6
NETMASK=255.255.0.0
# следующие два параметра можно не писать
# они вычисляются из IP и маски
BROADCAST=172.16.255.255
NETWORK=172.16.0.0
```

#### Инициализация VLAN на eth1
```
DEVICE=eth1.72
VLAN=yes
VLAN_TRUNK_IF=eth1
BOOTPROTO=static
IPADDR=10.10.0.1
NETMASK=255.255.255.192
ONBOOT=yes
```
