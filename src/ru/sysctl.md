---
layout: post
title:  "Sysctl"
date:   2019-03-30 29:39:00 +0300
slug: sysctl
categories: ru OS
locale: ru_RU
lang: ru
summary: "Оптимизация сервера, или ковыряние sysctl"
---

##### Параметры sysctl

Параметры, отвечающие за dirty pages — данные, которые нужно записать на диск или отправить в swap

Параметры значат:

|||
|----|----|
|`vm.dirty_background_ratio`| — процент системной памяти, который можно заполнить dirty pages до того, как фоновые процессы pdflush/flush/kdmflush запишут их на диск;|
|`vm.dirty_ratio`| — максимальный объем системной памяти, которую можно заполнить dirty pages;|
|`vm.dirty_background_bytes`|два предыдущих пункта, только в байтах; параметры взаимозаменяемы;|
|`vm.dirty_bytes`| — два предыдущих пункта, только в байтах; параметры взаимозаменяемы;|
|`vm.dirty_expire_centisecs`| — время, которое данные могут храниться в кэше, в нашем случае 30 с;|
|`vm.dirty_writeback_centisecs`| — как часто процессы pdflush/flush/kdmflush проверяют кэш.|

Количество данных, которые ожидают записи можно просмотреть так:

```bash

cat /proc/vmstat | egrep "dirty|writeback"

nr_dirty 1319
nr_writeback 0
nr_writeback_temp 0
# 1319 "грязных" страниц ожидает записи
```

Чтобы уменьшить размер кэша для уменьшения вероятности потери важных данных при сбое и минимизации возможных задержек записи/чтения необходимо отредактировать параметры vm.dirty_background_ratio и vm.dirty_ratio:

```bash

vm.dirty_background_ratio = 5
vm.dirty_ratio = 10
```
##### Значения записываются sysctl.conf

IPv6

Убедится что IPv6 не выключен

```bash

sudo sh -c 'echo 0 > /proc/sys/net/ipv6/conf/all/disable_ipv6'
```

Должно быть в файле /etc/sysctl.conf:

*Включение на всех интерфейсах*

```bash

net.ipv6.conf.all.disable_ipv6=0
```

*Включение на определенном интерфейсе*

```bash

net.ipv6.conf.eth0.disable_ipv6=0
```

Для применения новых параметров достаточно ввести sudo sysctl -p /etc/sysctl.conf

Не закончено

---

недописана

---

*Денис Валентинович*
