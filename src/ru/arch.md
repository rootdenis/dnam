---
layout: post
title:  "Arch"
date:   2020-01-09 16:45:00 +0300
slug: arch
categories: ru arch
locale: ru_RU
lang: ru
summary: "alfa: Arch linux"
---

### Install OS

#### Partitions

Создайте разделы при помощи `fdisk`:

```zsh
$ fdisk /dev/sda
```
Таблица дисков:

Type|Partition
---|---
`swap`|`/dev/sda1`
`/boot`|`/dev/sda2`
`/`|`/dev/sda3`

Форматирование:
```zsh
$ mkswap /dev/sda1
$ mkfs.btrfs /dev/sda2
$ mkfs.btrfs /dev/sda3
```

Примонтируейте корневой раздел
```zsh
$ mount /dev/sda3 /mnt
```

#### Installation

При помощи `pacstrap` ставим базовые пакеты:
```zsh
$ pacstrap /mnt base linux linux-firmware vim man-db man-pages texinfo iproute2 dhclient
```

#### Configure the system

##### Fstab

Генерируем `fstab`
```zsh
$ genfstab -U /mnt >> /mnt/etc/fstab
```

##### Chroot

`chroot` в установленную систему
```zsh
$ arch-chroot /mnt
```

##### Time zone

Ставим таймзону
```bash
$ ln -sf /usr/share/zoneinfo/Europe/Moscow /etc/localtime
$ hwclock --systohc
```

##### Localization

Uncomment `en_US.UTF-8 UTF-8` and other needed locales in `/etc/locale.gen`, and generate them with:
```bash
$ locale-gen
```
Set `LANG`:
```bash
$ echo 'LANG=en_US.UTF-8' > /etc/locale.conf
```

##### Network configuration

Hostname:
```bash
$ echo 'arch' > /etc/hostname
```

Set hosts:
```bash
cat >> /etc/hosts << EOF
127.0.0.1   localhost
::1         localhost
127.0.0.1   arch
EOF
```
##### Initramfs

Если есть `LVM, system encryption or RAID` выполнить:
```bash
$ mkinitcpio -P
```

##### Root password

```bash
$ passwd
```

##### Boot loader

[Здесь](https://wiki.archlinux.org/index.php/GRUB#GUID_Partition_Table_(GPT)_specific_instructions) описано подробнее про установку. 

При использовании типа `BIOS boot` для отдельного диска выполнить:
```bash
$ pacman -S grub
$ grub-install --target=i386-pc /dev/sdX
$ grub-mkconfig -o /boot/grub/grub.cfg
```
