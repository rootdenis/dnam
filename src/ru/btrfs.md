---
layout: post
title:  "BTRFS"
date:   2020-01-16 13:45:00 +0300
slug: btrfs
categories: ru fs
locale: ru_RU
lang: ru
summary: "Использование btrfs"
---

#### Новый диск

Форматируем диск:
```bash
$ mkfs.btrfs /dev/sdx
```

Маунтим диск для создания `subvolume`:
```bash
$ mount /dev/sdx /mnt
```

Создаем на диске необходимые `subvolume`:
```bash
$ btrfs subvolume create @storage
```

Создаем папку, в которую будем монтироваться наш сабвольюм:
```bash
$ mkdir /storage
```

Узнаем `UUID` диска:
```bash
$ blkid
/dev/sda1: UUID="5E22-0257" TYPE="vfat" PARTUUID="6a64b739-0810-4ca5-b515-5118fa6e15da"
/dev/sda2: UUID="82e2a215-fa04-4ad4-84f0-1431b9d508ff" TYPE="ext4" PARTUUID="b73c7f38-6d41-4bd7-98b8-2718ab47fa67"
/dev/sda3: UUID="5s6MiK-zyxM-159e-JmKG-F1Kp-Z25m-0kmub6" TYPE="LVM2_member" PARTUUID="4af440f2-54a7-4a17-a9c4-ff23e0424307"
/dev/mapper/ubuntu--vg-ubuntu--lv: UUID="f882a59f-c3cf-419c-a761-8acfc1957b7f" TYPE="ext4"
/dev/loop1: TYPE="squashfs"
/dev/loop2: TYPE="squashfs"
/dev/sdb: UUID="ce56ca79-2d21-45fd-9260-8c28d3e5c1ea" UUID_SUB="1a14c407-8680-4565-ab38-c8e0fdf8e960" TYPE="btrfs"
```

Теперь поправим `fstab`:
```
UUID=ce56ca79-2d21-45fd-9260-8c28d3e5c1ea /storage  btrfs   defaults,noatime,ssd,compress=lzo,commit=360,subvol=@storage  0       2
```

#### Расширение диска

После добавления места к блочному устройству надо выполнить соответствующую команду (в зависимости от того сколько надо отдать или забрать у раздела)

Отдать всё место разделу `/storage`
```bash
$ btrfs filesystem resize max /storage
```

Отдать 100Gb разделу `/storage`
```bash
$ btrfs filesystem resize +100g /storage
```
Забрать 100Gb у раздела `/storage`
```bash
$ btrfs filesystem resize -100g /storage
```

Всё делается на без остановки и не требует каких либо перезагрузок
