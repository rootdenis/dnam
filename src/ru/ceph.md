---
layout: post
title:  "Ceph"
date:   2019-11-13 14:37:00 +0300
slug: ceph
categories: ru fs cluster
locale: ru_RU
lang: ru
summary: "Заметки по ceph"
---

### Deploy ceph

#### Первоначальный подготовительный этап

Для начала необходимо добавить свои ssh ключи, чтобы деплоить `ceph` без проблем.

Теперь надо установить себе `ceph-deploy`
```bash
$ wget -q -O- 'https://download.ceph.com/keys/release.asc' | sudo apt-key add -
```

В данном случае [`{ceph-stable-release}`](https://docs.ceph.com/docs/master/start/quick-start-preflight/){:rel="nofollow" target="_blank"} - это релиз, который необходим для установки
```bash
$ sudo echo 'deb https://download.ceph.com/debian-{ceph-stable-release}/ $(lsb_release -sc) main' > /etc/apt/sources.list.d/ceph.list
```

```bash
$ sudo apt update
$ sudo apt install ceph-deploy
```

#### Установка ceph пакетов

При помощи команды установим ceph на наши машины:
```bash
$ ceph-deploy install {hostname [hostname] ...}
```

#### Создание конфигурации

Данная команда создаст конфигурационный файл для работы кластера
```bash
$ ceph-deploy new {host [host], ...}
```

#### Добавление мониторов

Чтобы сделать мониторы, используйте эту команду. Можно сделать один и более мониторов.
```bash
$ ceph-deploy mon create {host-name [host-name]...}
```

Для удаления мониторов можно использовать:
```bash
$ ceph-deploy mon destroy {host-name [host-name]...}
```

#### Ключи

Чтобы создать ключи для работы цефа надо юзать:
```bash
ceph-deploy gatherkeys {monitor-host}
```

Для того, чтобы задеплоить ключи на цеф ноды надо сделать:
```bash
$ ceph-deploy admin {monitor-host}
```

#### Подготовка дисков и OSD

Чтобы посмотреть, какие диски есть на машинах ceph кластера можно использовать:
```bash
$ ceph-deploy disk list [hostname]
```

Для подготовки дисков используйте:
```bash
$ ceph-deploy disk zap {osd-server-name} {disk-name}
$ ceph-deploy disk zap osdserver1 /dev/sdb /dev/sdc
```

Для создания osd:
```bash
$ ceph-deploy osd create --data {data-disk} {node-name}
$ ceph-deploy osd create --data /dev/ssd osd-server1
```

Чтобы посмотреть список osd:
```bash
$ ceph-deploy osd list {node-name}
```

#### Установка mgr

Сначала надо создать аутентификационный ключ. Для это выберем ноду для инсталяции и введем
`$name` - имя сервера, на котором разворачивается `mgr`.
```bash
$ mkdir
$ ceph auth get-or-create mgr.$name mon 'allow profile mgr' osd 'allow *' mds 'allow *' > /var/lib/ceph/mgr/$name/keyring
```

Теперь запустим менеджер:
```bash
$ ceph-mgr -i $name
```

#### Удаление конфигурации и пакетов

Для удаления пакетов можно использовать 
```bash
$ ceph-deploy purge <your_servers>
```

Для удаления данных можно использовать 
```bash
$ ceph-deploy purgedata <your_servers>
```

### Работа с ceph

#### Создание пула

Чтобы создать пул:
```bash
$ ceph osd pool create $name $pg_num $pgp_num
```

- `$name` - название пула
- `$pg_num` and `$pgp_num` - Когда кластер Ceph получает запросы на хранение данных, он разделяет их на разделы, называемые группами размещения (PG, placement groups). Тем не менее, данные CRUSH сначала разбиваются на набор объектов и на основании хеш- операций с именами объектов, уровнями репликаций и общего числа групп размещения в системе создаются идентификаторы групп размещения. Группа размещения является логической коллекцией объектов, которые реплицируются в OSD (устройства хранения объектов) для обеспечения надёжности в системе хранения. В зависимости от уровня репликаций в вашем пуле Ceph, с каждой группой размещения выполняется репликация распространяемая на более чем одно OSD в кластере Ceph. Вы можете рассматривать группу размещения как логический контейнер, хранящий множество объектов таким образом, что этот логический контейнер отображается на множество OSD. Группы размещения имеют важное значение для масштабируемости и производительности системы хранения Ceph. Вычисляется по [рекомендациям ceph](https://docs.ceph.com/docs/jewel/rados/operations/placement-groups/#set-the-number-of-placement-groups).


### Ссылки
- [PREFLIGHT CHECKLIST](https://docs.ceph.com/docs/master/rados/deployment/preflight-checklist/){:rel="nofollow" target="_blank"}
- [INSTALL](https://docs.ceph.com/docs/master/rados/deployment/ceph-deploy-install/#){:rel="nofollow" target="_blank"}
- [CREATE A CLUSTER](https://docs.ceph.com/docs/master/rados/deployment/ceph-deploy-new/){:rel="nofollow" target="_blank"}
- [ADD/REMOVE MONITORS](https://docs.ceph.com/docs/master/rados/deployment/ceph-deploy-mon/){:rel="nofollow" target="_blank"}
- [KEYS MANAGEMENT](https://docs.ceph.com/docs/master/rados/deployment/ceph-deploy-keys/){:rel="nofollow" target="_blank"}
- [Группы размещения](http://onreader.mdl.ru/LearningCeph/content/Ch04.html){:rel="nofollow" target="_blank"}
