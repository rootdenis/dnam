---
layout: post
title:  "Nexus docker registry"
date:   2019-12-04 15:17:00 +0300
slug: nexusdocker
categories: ru DevOps Nexus
locale: ru_RU
lang: ru
summary: "Приватный Docker registry в Nexus"
---


### Почему

Боль и страдания, которые пришлось испытать при работе с протусом переполнили чашу терпения, а использовать стандартный `docker regestry` с портом `5000` не хотелось. Да и Nexus уже был как прокси для `Apt` пакетов, поэтому решил настроить по уже знакомой схеме.

![Nexus Icon](/images/nexus/nexusrepo.png "Nexus")

### Как

Собственно надо просто выкачать репозиторий и запутить `docker-compose` (SSL лучше сгенерить, чтобы `docker login` работал и не думать, почему же он не работает)

```yaml
version: '3.1'

services:
  artifactory:
     image: sonatype/nexus3:latest
     build: .
     container_name: Artifactory
     env_file: .env
     volumes:
         - /etc/localtime:/etc/localtime:ro
         - ./data/artifactory:/nexus-data
     restart: unless-stopped
     network_mode: host

  nginx:
     image: nginx:alpine
     container_name: Nginx
     env_file: .env
     volumes:
         - ./data/nginx:/etc/nginx/conf.d
     restart: unless-stopped
     network_mode: host
```

`artifactory.conf`:

```text
proxy_send_timeout 900;
proxy_read_timeout 900;
proxy_buffering off;
#keepalive_timeout 5 5;
tcp_nodelay on;

upstream artifactory_server { server 127.0.0.1:8081 fail_timeout=0; }
upstream registry_server { server 127.0.0.1:5000 fail_timeout=0; }

### SSL
server {
    listen 443 ssl http2;
    listen [::]:443 ssl http2;
    server_name nexus.dtulyakov.ru

    server_tokens off;

    ssl_certificate /etc/nginx/ssl/nginx.crt;
    ssl_certificate_key /etc/nginx/ssl/nginx.key;
    ssl_dhparam /etc/nginx/ssl/dhparam.pem;

    ssl_session_timeout 1h;
    ssl_session_cache shared:SSL:10m;
    ssl_protocols TLSv1 TLSv1.1 TLSv1.2;

    ssl_prefer_server_ciphers on;
    ssl_ciphers AES256+EECDH:AES256+EDH:!aNULL;

    reset_timedout_connection on;
    charset UTF-8;
    client_max_body_size 0;
    location / {
        #return 301 http://$host$request_uri;
        proxy_read_timeout 900;
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto "https";
        proxy_redirect http:// https://;
        proxy_pass http://artifactory_server/;
    }
}
### SSL

server {
  listen 80;
  listen [::]:80;
  server_name nexus.dtulyakov.ru
  reset_timedout_connection on;
  charset UTF-8;
  location / {
        #return 301 https://$host$request_uri;
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto "http";
        proxy_redirect https:// http://;
        proxy_pass http://artifactory_server/;
  }
}

server {
    listen 443 ssl http2;
    listen [::]:443 ssl http2;
    server_name registry.dtulyakov.ru

    server_tokens off;

    ssl_certificate /etc/nginx/ssl/nginx.crt;
    ssl_certificate_key /etc/nginx/ssl/nginx.key;
    ssl_dhparam /etc/nginx/ssl/dhparam.pem;

    ssl_session_timeout 1h;
    ssl_session_cache shared:SSL:10m;
    ssl_protocols TLSv1 TLSv1.1 TLSv1.2;

    ssl_prefer_server_ciphers on;
    ssl_ciphers AES256+EECDH:AES256+EDH:!aNULL;

    reset_timedout_connection on;
    charset UTF-8;
    client_max_body_size 0;
    location / {
        #return 301 http://$host$request_uri;
        proxy_read_timeout 900;
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto "https";
        proxy_redirect http:// https://;
        proxy_pass http://registry_server/;
    }
}
```

Далее надо создать репозиторий `docker hosted` с параметрами по умолчанию (единственное я добавил порт `5000`)

![DockerRegistry](/images/nexus/nexus-docker-repo.png "Docker registry")

### Ссылки

- [Nexus docker-compose](https://gitlab.com/dtulyakov/docker-nexus){:rel="nofollow" target="_blank"}
