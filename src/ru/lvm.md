---
layout: post
title:  "LVM"
date:   2019-11-26 13:20:00 +0300
slug: lvm
categories: ru fs
locale: ru_RU
lang: ru
summary: "Заметки по LVM"
---

### How to delete a volume group in LVM

The post describes steps to delete a volume group. Make sure you have unmounted and the mount points and taken prior backup of the mount point if required.

1. Once you have umounted all the mount points, you can remove the LVs associated with them. To do so use the lvremove command :

```bash
$ lvremove -f [vg_name]/[lv_name]
```

2. To remove the Volume group we have to deactivate it first with vgchange command :

```bash
$ vgchange -an [vg_name]
```

3. You can remove the VG now.

```bash
$ vgremove [vg_name]
```

4. To remove physical volumes in the VG use following command:

```bash
$ pvremove [pv_name]
```
