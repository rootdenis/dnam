---
layout: post
title: "Оптимизирование картинок под web"
date: 2016-06-27 15:15:10 +0300
slug: optimization-images
categories: ru web
locale: ru_RU
lang: ru
summary: "Гугл проверяльщик (скорости и оптимизации сайта) часто ругается на неоптимизированные картинки. Попытки побороть тупизну контента не увенчались успехом, посему победить решил через крон и оптимизатор картинок."
---


###### Вариант 1 #

Для начала надо установить на сервере jpegoptim

```bash
sudo apt install optipng
```

Затем настроить автоматическую оптимизацию картинок

```bash
#[минуты] (0-59)
#|   [часы] (0-23)
#|   |    [числа месяца] (1-31)
#|   |    |    [месяцы] (1-12)
#|   |    |    |    [дни_недели] (0-6 with 0=Sun)
#|   |    |    |    |    [юзер](only system cron)
#|   |    |    |    |    |    [команда]
#|   |    |    |    |    |    |
0    3    *    *    *        /usr/bin/find /var/www/sites/default/files/ -iname '*.png' -print0 |xargs -0 optipng -o7
#EOF
```

###### Вариант 2 #

```bash
sudo apt install pngcrush
```

Наилучшее сжатие

```bash
pngcrush -rem alla -rem text -reduce -brute in.png out.png
```

Автоматическая оптимизация

```bash
pngcrush -reduce -brute in.png out.png
```

Убрать все данные из файла о цветовой коррекции

```bash
pngcrush -rem gAMA -rem cHRM -rem iCCP -rem sRGB in.png out.png
```

Убрать вспомогательные данные

```bash
pngcrush -rem alla -rem text in.png out.png
```

Оптимизировать все файлы в цикле

```bash
for file in *.png ; do pngcrush -reduce -brute -rem alla -rem gAMA -rem cHRM -rem iCCP -rem sRGB "$file" "${file%.png}-crushed.png" && mv "${file%.png}-crushed.png" "$file" ; done
```

Для начала надо установить на сервере jpegoptim

```bash
sudo apt install jpegoptim
```

Затем настроить автоматическую оптимизацию картинок

```bash
#[минуты] (0-59)
#|   [часы] (0-23)
#|   |    [числа месяца] (1-31)
#|   |    |    [месяцы] (1-12)
#|   |    |    |    [дни_недели] (0-6 with 0=Sun)
#|   |    |    |    |    [юзер](only system cron)
#|   |    |    |    |    |    [команда]
#|   |    |    |    |    |    |
0    3    *    *    *        /usr/local/bin/jpegoptim --strip-all /usr/local/www/sites/default/files/*.jpg
#EOF
```
