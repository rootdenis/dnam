---
layout: post
title:  "Company structure"
date:   2019-07-11 10:54:00 +0300
slug: company-structure
categories: ru IT
locale: ru_RU
lang: ru
summary: "Расшифровка должностей"
---

* `CEO` (Chief Executive Officer) дословно — главный исполнительный директор — высшее должностное лицо компании (генеральный директор, председатель правления, президент, руководитель). Определяет общую стратегию предприятия, принимает решения на высшем уровне, выполняет представительские обязанности.
* `CFO` (Chief Financial Officer) — финансовый директор — один из высших управленцев компании, ответственный за управление финансовыми потоками бизнеса, за финансовое планирование и отчётность. Определяет финансовую политику организации, разрабатывает и осуществляет меры по обеспечению ее финансовой устойчивости. Руководит работой по управлению финансами исходя из стратегических целей и перспектив развития организации, по определению источников финансирования с учетом рыночной конъюнктуры. В типичной схеме управления компанией занимает пост вице-президента по финансам и подотчётен президенту компании или генеральному директору. Часто является членом совета директоров.
* `СVO` (Chief Visionary Officer) — исполнительный директор, один из высших управленцев компании. В типичной схеме управления компанией занимает пост вице-президента и подотчётен президенту компании или генеральному директору. Часто является членом совета директоров.
* `COO` (Chief Operating Officer) — главный операционный директор; один из руководителей учреждения, отвечающий за повседневные операции, за текущую деятельность. В русском языке и бизнесе этому понятию соответствует должность «исполнительный директор».
* `CIO` (Chief Information Officer) — менеджер по информатизации (главный), директор по информационным технологиям (сотрудник корпорации, исполнитель высшего ранга, отвечающий за приобретение и внедрение новых технологий, управление информационными ресурсами). Наиболее точно данному понятию на русском языке соответствуют «IT-директор», «Директор департамента информационных технологий», «Заместитель генерального директора по ИТ».
* `CSO` (Chief Security Officer) — руководитель отдела безопасности, (главный) директор по безопасности организации.
* `CISO` (Chief Information Security Officer) — руководитель отдела IT-безопасности, (главный) директор по IT-безопасности. CISO может подчиняться как CIO, так и CSO.
* `CMO` (Chief Marketing Officer) — директор по маркетингу/коммерческий директор, руководитель, относящийся к категории топ-менеджмента, высшего руководства предприятия. Определяет маркетинговую стратегию предприятия, принимает решения на высшем уровне, руководит работой маркетинговой службы предприятия.
* `CAO` (Chief Accounting Officer) — главный бухгалтер. Руководитель, относящийся к категории топ-менеджмента, высшего руководства предприятия. Отвечает за все аспекты бухгалтерской работы.
* `CTO` (Chief technical officer или Chief technology officer) — технический директор — руководящая должность в западных компаниях, соответствует русскому «главный инженер». Один из руководителей корпорации, отвечающий за её развитие и разработку новых продуктов; в его ведении обычно находится вся технологическая часть производства.


---

*Денис Валентинович*
