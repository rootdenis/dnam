---
layout: post
title:  "Настройка alternatives"
date:   2015-06-08 11:50:00 +0300
slug: alternatives
categories: ru OS
locale: ru_RU
lang: ru
summary: Настройка alternatives в Ubuntu Linux
---


В **прочтименя** на гитхабе автора есть примеры установки и запуска

Для примера, что бы  поменять дефлтный python с версии python2 на версию python3

```bash

update-alternatives --install /usr/bin/python python /usr/bin/python3 10
```

Передумали и решили 2,7 заюзать и при этом вес уменьшить

```bash

update-alternatives --install /usr/bin/python python /usr/bin/python27 9
```

повыбирать можно тут:

```bash

update-alternatives --config python
```
