---
layout: post
title:  "Gitlab"
date:   2019-12-27 13:40:00 +0300
slug: gitlab
categories: ru VCS CICD
locale: ru_RU
lang: ru
summary: "Памятки по gitlab"
---

### Gitlab badges

#### Pipeline

Link:

`https://gitlab.com/%{project_path}/%{default_branch}/pipeline.svg`

Badge image URL:

`https://gitlab.com/%{project_path}/badges/%{default_branch}/pipeline.svg`


### GitLab CI/CD environment variables

```
export CI_JOB_ID="50"
export CI_COMMIT_SHA="1ecfd275763eff1d6b4844ea3168962458c9f27a"
export CI_COMMIT_SHORT_SHA="1ecfd275"
export CI_COMMIT_REF_NAME="master"
export CI_REPOSITORY_URL="https://gitlab-ci-token:abcde-1234ABCD5678ef@example.com/gitlab-org/gitlab-foss.git"
export CI_COMMIT_TAG="1.0.0"
export CI_JOB_NAME="spec:other"
export CI_JOB_STAGE="test"
export CI_JOB_MANUAL="true"
export CI_JOB_TRIGGERED="true"
export CI_JOB_TOKEN="abcde-1234ABCD5678ef"
export CI_PIPELINE_ID="1000"
export CI_PIPELINE_IID="10"
export CI_PAGES_DOMAIN="gitlab.io"
export CI_PAGES_URL="https://gitlab-org.gitlab.io/gitlab-foss"
export CI_PROJECT_ID="34"
export CI_PROJECT_DIR="/builds/gitlab-org/gitlab-foss"
export CI_PROJECT_NAME="gitlab-foss"
export CI_PROJECT_TITLE="GitLab FOSS"
export CI_PROJECT_NAMESPACE="gitlab-org"
export CI_PROJECT_PATH="gitlab-org/gitlab-foss"
export CI_PROJECT_URL="https://example.com/gitlab-org/gitlab-foss"
export CI_REGISTRY="registry.example.com"
export CI_REGISTRY_IMAGE="registry.example.com/gitlab-org/gitlab-foss"
export CI_REGISTRY_USER="gitlab-ci-token"
export CI_REGISTRY_PASSWORD="longalfanumstring"
export CI_RUNNER_ID="10"
export CI_RUNNER_DESCRIPTION="my runner"
export CI_RUNNER_TAGS="docker, linux"
export CI_SERVER="yes"
export CI_SERVER_URL="https://example.com"
export CI_SERVER_HOST="example.com"
export CI_SERVER_PORT="443"
export CI_SERVER_PROTOCOL="https"
export CI_SERVER_NAME="GitLab"
export CI_SERVER_REVISION="70606bf"
export CI_SERVER_VERSION="8.9.0"
export CI_SERVER_VERSION_MAJOR="8"
export CI_SERVER_VERSION_MINOR="9"
export CI_SERVER_VERSION_PATCH="0"
export GITLAB_USER_EMAIL="user@example.com"
export GITLAB_USER_ID="42"
```

### Gitlab runner in docker

#### Запуск раннера в докере

Данная схема позволяет использовать раннер в докере, при этом внутри можно использовать докер для того, чтобы билдить образы либо выполнять билд при помощи образов докера.

Используемый `docker-compose.yaml`:

```yaml
version: "3.1"
services:
 gitlab-runner:
   image: gitlab/gitlab-runner:latest
   container_name: gitlab-runner
   restart: unless-stopped
   volumes:
           - /etc/gitlab-runner:/etc/gitlab-runner
           - /var/run/docker.sock:/var/run/docker.sock
```

Файл `/etc/gitlab-runner/config.toml`.

```
[[runners]]
  name = "gitlab-runner"
  url = "https://gitlab.example.com/"
  token = xxxxxxxxxxx
  executor = "docker"
  [runners.custom_build_dir]
  [runners.docker]
    tls_verify = false
    image = "docker:latest"
    privileged = true
    disable_entrypoint_overwrite = false
    oom_kill_disable = false
    disable_cache = false
    volumes = ["/cache:/cache", "/dev/shm", "/var/run/docker.sock:/var/run/docker.sock"]
    shm_size = 0
  [runners.cache]
    [runners.cache.s3]
    [runners.cache.gcs]
```

`Image` можно использовать любой

#### Маунт папки внутри докера

Для того, чтобы смонтировать папку, которая находится как бы внутри докера, необходимо вычислить ее местонахождение на хосте. Для этого, надо добавить в `.gitlab-ci.yml` следующие строки:

```yaml
 script:
   - DOCKER_CONTAINER_ID=$(docker container ps --filter name=$HOSTNAME --format "{{.ID}}")
   - DOCKER_VOLUME_SOURCE=$(docker container inspect "$DOCKER_CONTAINER_ID"
```


#### Использование ssh в .gitlab-ci

В секцию `before_script` надо добавить:
```yaml
before_script:
  - apk --update add openssh-client &&
  - which ssh-agent || apk add --update openssh-client &&
  - eval $(ssh-agent -s)
  - echo "$DEPLOY_SSH_PRIVATE_KEY" | tr -d '\r' | ssh-add - > /dev/null
  - mkdir -p ~/.ssh
  - chmod 700 ~/.ssh
  - '[[ -f /.dockerenv ]] && echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config'
```

- `$DEPLOY_SSH_PRIVATE_KEY` - приватный ключ, для доступа по ssh.


### Сбрасывание токенов при переносе Gtilab'а

Бывает так, что при восстановлении из бэкапа может быть утерян `gitlab-secret.json`. В такоем случае  необходимо 
резетнуть все токены, которые хранятся в зашифрованной виде. Для этого делаем:
```bash
$ gitlab-rails dbconsole
gitlabhq_production=> UPDATE projects SET runners_token = null, runners_token_encrypted = null;
gitlabhq_production=> UPDATE namespaces SET runners_token = null, runners_token_encrypted = null;
gitlabhq_production=> UPDATE application_settings SET runners_registration_token_encrypted = null;
gitlabhq_production=> UPDATE ci_runners SET token = null, token_encrypted = null;
```
