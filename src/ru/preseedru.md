---
layout: post
title:  "Авто установка debian"
date:   2020-10-26 12:31:09 +0300
slug: preseed
categories: ru debian
locale: ru_RU
lang: ru
summary: "Auto установка debian с помощью preseed файла"
---

Автоустановка debian с помощью preseed файла. [en](/en/preseed)

Во время загрузки надо выбрать `Advanced options`

<img src="/images/preseed/preseed-01.png" style="width: 800.0px;">

[Скрин 1][1]

Затем выбрать `Automated install`

<img src="/images/preseed/preseed-02.png" style="width: 800.0px;">

[Скрин 2][2]

В появившемся поле надо ввести ресурс где находится автоконфиг `auto.loc`

![Auto-03](/images/preseed/preseed-03.png "Auto 03")

[Скрин 3][3]

Рабочий конфиг используемый на `auto.loc`

```bash
#### Contents of the preconfiguration file (for bullseye)
### Localization
d-i debian-installer/language string en
d-i debian-installer/country string RU
d-i debian-installer/locale string en_GB.UTF-8
d-i localechooser/supported-locales multiselect en_US.UTF-8, ru_RU.UTF-8

# Keyboard selection.
d-i keyboard-configuration/xkb-keymap select us

### Network configuration
d-i netcfg/choose_interface select auto
d-i netcfg/get_hostname string debianhost
d-i hw-detect/load_firmware boolean true

### Mirror settings
# If you select ftp, the mirror/country string does not need to be set.
#d-i mirror/protocol string ftp
d-i mirror/country string manual
d-i mirror/http/hostname string mirror.yandex.ru
d-i mirror/http/directory string /debian
d-i mirror/http/proxy string http://10.0.0.10:8118/

### Account setup
# Skip creation of a root account (normal user account will be able to use sudo).
d-i passwd/root-login boolean false
d-i passwd/make-user boolean true
d-i passwd/user-fullname string Ansible user
d-i passwd/username string ansible
#d-i passwd/user-password password fuckyou
#d-i passwd/user-password-again password fuckyou
d-i passwd/user-password-crypted password $6UI.......................lIOop1
d-i passwd/user-uid string 1000

### Clock and time zone setup
d-i clock-setup/utc boolean true
d-i time/zone string Europe/Moscow

# Controls whether to use NTP to set the clock during the install
d-i clock-setup/ntp boolean true
# NTP server to use. The default is almost always fine here.
d-i clock-setup/ntp-server string pool.ntp.org

#### Partitioning
### Partitioning example
#
## You can define the amount of space that will be used for the LVM volume
## group. It can either be a size with its unit (eg. 20 GB), a percentage of
## free space or the 'max' keyword.
d-i partman-auto-lvm/new_vg_name string sys
d-i partman-auto-lvm/guided_size string max
d-i partman-lvm/device_remove_lvm boolean true
d-i partman-md/device_remove_md boolean true
d-i partman-lvm/confirm boolean true
d-i partman-lvm/confirm_nooverwrite boolean true
d-i partman-partitioning/confirm_write_new_label boolean true
d-i partman/choose_partition select finish
d-i partman/confirm boolean true
d-i partman/confirm_nooverwrite boolean true

# Partitioning
# this only makes partman automatically partition without confirmation:
d-i     partman-partitionining/confirm_write_new_label  boolean true
d-i     partman-md/confirm_nooverwrite  boolean true
d-i     mdadm/boot_degraded             boolean true

# Partitioning
d-i partman-auto/method string raid
d-i partman-auto/disk string /dev/sda /dev/sdb

d-i partman-auto/expert_recipe string multiraid :: \
    100      10   100  raid $primary{ } $lvmignore{ } method{ raid } .\
    512      20   512  raid $primary{ } $lvmignore{ } method{ raid } .\
    900      25    -1  raid $primary{ } $lvmignore{ } method{ raid } . \
    30000    50    -1  btrfs $defaultignore{ } $lvmok{ } lv_name{ root } method{ format } format{ }  use_filesystem{ }  filesystem{ btrfs }  mountpoint{ / } . \
    50000    50    -1  ext4 $defaultignore{ } $lvmok{ } lv_name{ home } method{ format } format{ }  use_filesystem{ }  filesystem{ ext4 }  mountpoint{ /home } . \
    10000    50    -1  ext4 $defaultignore{ } $lvmok{ } lv_name{ tmp } method{ format } format{ }  use_filesystem{ }  filesystem{ ext4 }  mountpoint{ /tmp } . \
    100000   50    -1  ext4 $defaultignore{ } $lvmok{ } lv_name{ var } method{ format } format{ }  use_filesystem{ }  filesystem{ xfs }  mountpoint{ /var } .
#
#2000     50    -1  btrfs $defaultignore{ } \
#    $lvmok{ } \
#    lv_name{ root } \
#    method{ format } \
#    format{ }  \
#    use_filesystem{ }  \
#    filesystem{ btrfs }  \
#    mountpoint{ / } \
#    options/discard{ discard } \
#    options/relatime{ relatime } \
#    options/ssd{ ssd }. \
#
# specify how the previously defined partitions will be
# used in the RAID setup.
d-i partman-auto-raid/recipe string  \
    1 2 0 fat32 /boot/efi /dev/sda1#/dev/sdb1 . \
    1 2 0 ext4 /boot /dev/sda2#/dev/sdb2 . \
    1 2 0 lvm - /dev/sda3#/dev/sdb3 .
    #1 2 0 btrfs / /dev/sda3#/dev/sdb3 .

# This makes partman automatically partition without confirmation.
d-i partman-md/confirm boolean true
d-i partman-partitioning/confirm_write_new_label boolean true
d-i partman/choose_partition select finish
d-i partman/confirm boolean true
d-i partman/confirm_nooverwrite boolean true
d-i partman-basicfilesystems/no_swap boolean false

## Controlling how partitions are mounted
d-i partman/mount_style select label

### Apt setup
# You can choose to install non-free and contrib software.
d-i apt-setup/non-free boolean true
d-i apt-setup/contrib boolean true

### Package selection
tasksel tasksel/first multiselect
tasksel/skip-tasks multiselect server

# Individual additional packages to install
d-i pkgsel/include string openssh-server build-essential openssh-server python-apt

### Boot loader installation
d-i grub-installer/only_debian boolean true
d-i grub-installer/with_other_os boolean true
d-i grub-pc/install_devices multiselect /dev/sda, /dev/sdb
d-i grub-installer/bootdev  string /dev/sda /dev/sdb
d-i finish-install/reboot_in_progress note
popularity-contest popularity-contest/participate boolean false

#### Advanced options
### Running custom commands during the installation
d-i preseed/late_command string apt-install zsh; \
    in-target /bin/sh -c "echo 'dummy' >> /etc/modules"; \
    in-target mkdir -p /home/ansible/.ssh; \
    in-target /bin/sh -c "echo 'ssh-rsa AAAAB3Nz..................pOUle0YhKxJ Ansible user' >> /home/ansible/.ssh/authorized_keys"; \
    in-target /bin/sh -c "echo 'ansible ALL=(ALL) NOPASSWD: ALL' >> /etc/sudoers"; \
    in-target chown -R ansible:ansible /home/ansible; \
    in-target chmod -R 600 /home/ansible/authorized_keys; \
    in-target sh -c 'eject || true';
d-i debian-installer/splash boolean false
d-i cdrom-detect/eject boolean true

### Shutdown machine
d-i finish-install/reboot_in_progress note
d-i debian-installer/exit/halt boolean false
d-i debian-installer/exit/poweroff boolean false

```

# Links
- [auto.loc][auto]{:rel="nofollow" target="_blank"}
- [Скрин 1][1]
- [Скрин 2][2]
- [Скрин 3][3]

[1]:{{ site.url }}/images/preseed/preseed-01.png
[2]:{{ site.url }}/images/preseed/preseed-02.png
[3]:{{ site.url }}/images/preseed/preseed-03.png
[auto]: https://auto.loc/
