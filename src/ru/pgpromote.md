---
layout: post
title:  "Как переключить слейв в режим мастера. PostgreSQL"
date:   2023-04-06 16:08:10 +0200
slug: pgpromote
categories: ru SQL Postgresql
locale: ru_RU
lang: ru
summary: "Как переключить слейв в режим мастера. Postgresql"
---

## Что бы слей переключить в мастер режим надо сделать следующее

### Узнать путь к каталогу

```sql
postgres=# show data_directory;
     data_directory
------------------------
 /storage/postgresql/db
(1 row)
```

### Добавить в переменное окружение этот путь

```bash
export PGDATA="/storage/postgresql/db"
```

### И переключить базу в режим мастера

```bash
/usr/lib/postgresql/13/bin/pg_ctl promote
```

