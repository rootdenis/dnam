---
layout: post
title:  "Nvidia CUDA & Docker or Podman"
date:   2020-07-08 07:42:00 +0300
slug: nvidia-docker
categories: ru containers
locale: ru_RU
lang: ru
summary: "Настройка Docker и Podman для работы с Nvidia CUDA"
---


![CUDA](/images/nvidia/CUDA.png "CUDA")

### Что это такое

- Nvidia CUDA (Compute Unified Device Architecture) - программно-аппаратная архитектура параллельных вычислений, которая позволяет существенно увеличить вычислительную производительность благодаря использованию графических процессоров фирмы Nvidia.
- Docker - программное обеспечение для автоматизации развёртывания и управления приложениями в средах с поддержкой контейнеризации. Позволяет «упаковать» приложение со всем его окружением и зависимостями в контейнер, который может быть перенесён на любую Linux-систему с поддержкой cgroups в ядре, а также предоставляет среду по управлению контейнерами. Изначально использовал возможности LXC, с 2015 года применял собственную библиотеку, абстрагирующую виртуализационные возможности ядра Linux — libcontainer. С появлением Open Container Initiative начался переход от монолитной к модульной архитектуре.
- Podman - What is Podman? Podman is a daemonless container engine for developing, managing, and running OCI Containers on your Linux System. Containers can either be run as root or in rootless mode. Simply put: `alias docker=podman`.

### Требования

Для того что бы уйти от проблем docker демона, можно использовать podman.

Для возможности запуска podman с CUDA в контейнере необходимо сделать некоторые манипуляции.

Инструкция для ubuntu 18.04

#### Установка nvidia драйвера

```bash
distribution=$(. /etc/os-release;echo $ID$VERSION_ID)
curl -s -L https://nvidia.github.io/nvidia-docker/gpgkey | sudo apt-key add -
curl -s -L https://nvidia.github.io/nvidia-docker/$distribution/nvidia-docker.list | sudo tee /etc/apt/sources.list.d/nvidia-docker.list
sudo apt install -y nvidia-container-toolkit
sudo systemctl restart docker
```

### Параметры запуска docker

```bash
docker run --privileged --rm --gpus all dtulyakov/cudatest
```

Если не был указан параметр `no-cgroups = true`

```bash
docker run --rm --gpus all dtulyakov/cudatest
```

#### Настройка nvidia hooks для podman

```bash
sudo add-apt-repository -y ppa:projectatomic/ppa
sudo apt install podman buildah skopeo nvidia-container-runtime-hook
```

```bash
cat <<EOF >> /usr/share/containers/oci/hooks.d/oci-nvidia-hook.json
{
  "hook": "/usr/bin/nvidia-container-runtime-hook",
  "arguments": ["prestart"],
  "annotations": ["sandbox"],
  "stage": [ "prestart" ]
}
EOF
```

```bash
cat <<EOF >> /etc/nvidia-container-runtime/config.toml
disable-require = false
#swarm-resource = "DOCKER_RESOURCE_GPU"

[nvidia-container-cli]
#root = "/run/nvidia/driver"
#path = "/usr/bin/nvidia-container-cli"
environment = []
#debug = "/var/log/nvidia-container-runtime-hook.log"
#ldcache = "/etc/ld.so.cache"
load-kmods = true
no-cgroups = true
#user = "root:video"
ldconfig = "@/sbin/ldconfig.real"
EOF
```
Если указать этот параметр `no-cgroups = true` то придётся запускать контейнер в привелигерованном режиме!

### Параметры запуска podman

```bash
podman run --rm --privileged dtulyakov/cudatest
```
Если не был указан параметр `no-cgroups = true`
```bash
podman run --rm dtulyakov/cudatest
```

### Ссылки

- [Nvidia-docker](https://github.com/nvidia/nvidia-docker){:rel="nofollow" target="_blank"}
