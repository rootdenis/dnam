---
layout: post
title:  "Настройка logrotat"
date:   2020-10-26 19:30:10 +0300
slug: logrotatebsd
categories: ru System LogRotate FreeBSD Ubuntu Linux
locale: ru_RU
lang: ru
summary: "Как настроить logrotat"
---

##### Как настроить logrotat

Настройка происходит на FreeBSD:


```bash
make -C /usr/ports/sysutils/logrotate/ install clean
```

```bash
cp /usr/local/etc/logrotate.conf.sample /usr/local/etc/logrotate.conf
```

```bash
weekly
rotate 4
# отправлять errorsы root
errors root
create
compress
include /usr/local/etc/logrotate.d

/var/log/lastlog {
    monthly
    rotate 1
}
```

Если планируется не отказываться от стандартного newsyslog - то тогда надо убрать лишнее из конфига

```bash
/var/log/lastlog {
    monthly
    rotate 1
}
```

Создаём каталог если его нету

```bash
mkdir /usr/local/etc/logrotate.d
```

и кладём туда конфиг например для nginx

```bash
vi /usr/local/etc/logrotate.d/nginx
```

```bash
/var/log/nginx/*.log {
        daily
        missingok
        rotate 30
        compress
        delaycompress
        notifempty
        create 640 root
        sharedscripts
        postrotate
                [ ! -f /var/run/nginx.pid ] || kill -USR1 `cat /var/run/nginx.pid`
        endscript
}
```

* daily – ротацию проводить раз в день
* weekly – ротацию проводить раз в неделю
* size=16M - максимальный размер несжатого файла; пока размер текущего
* rotate 30 – число отротейтченых файлов, (dtulyakov.ru.access.log будет содержать 30 файлов, при превышении этого числа самый старый будет заменятся.)
* missingok – отсутствие файла не является ошибкой
* compress – сжимать файлы при ротации
* delaycompress – первый ротированный файл не сжимать
* nodelaycompress - не откладывать сжатие файла на следующий цикл
* notifempty – уведомлять о том что файл был пустой.
* create 640 root – с какими правами и владельцем будет создан новый файл
* nocreate - не создавать пустой журнал
* nocopytruncate - не сбрасывать файл журнала после копирования
* copytruncate - сбрасывать файл журнала после копирования
* nomail - не отправлять содержимое удаляемых (старых) журналов по почте
* noolddir - держать все файлы в одном и том же каталоге
* olddir - держать старые файлы в другом каталоге
* sharedscripts – выполнять действия описанные в postrotate 1 раз, а не на каждый файл
* postrotate - что делать после ротации
* endscript - Между postrotate и endscript расположены команды интерпретатора sh(1), исполняемые непосредственно после ротации.



```bash
crontab -e
```



```bash
30       *       *       *       *       /usr/local/sbin/logrotate /usr/local/etc/logrotate.conf
```
