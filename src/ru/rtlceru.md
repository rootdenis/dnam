---
layout: post
title:  "Wi-Fi и ASUS TUF Gaming FX505DY"
date:   2020-10-26 12:31:10 +0300
slug: rtl8821ce
categories: ru debian ubuntu rtl8821ce
locale: ru_RU
lang: ru
summary: "Как установить драйвер Wi-Fi для ASUS TUF Gaming FX505DY"
---

##### Как установить драйвер Wi-Fi для ASUS TUF Gaming FX505DY. [en](/en/rtl8821ce)

Если ты счастливый обладатель ноутбука asus - это для тебя. И если ты так же счастливый обладатель ubuntu - это определённо для тебя.

```bash

sudo apt-get install --reinstall git dkms build-essential linux-headers-$(uname -r)
git clone https://github.com/tomaspinho/rtl8821ce
cd rtl8821ce
chmod +x dkms-install.sh
chmod +x dkms-remove.sh
sudo ./dkms-install.sh
 
```


### Links
- [ASUS TUF Gaming FX505DY][FX505DY]{:rel="nofollow" target="_blank"}

[FX505DY]: https://www.asus.com/ru/Laptops/ASUS-TUF-Gaming-FX505DY/
