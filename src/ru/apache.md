---
layout: post
title:  "Apache"
date:   2020-04-14 13:31:00 +0300
slug: apache
categories: ru Apache
locale: ru_RU
lang: ru
summary: "Заметки по Apache"
---

### Share folder with basic auth

Создайте при помощи `htpasswd -c /etc/.htpasswd <your_user>` файл с паролем для пользователя. Теперь добавьте в ваш файл конфигурции следующую секцию:
```
    Alias /logs /var/log
    <Directory /var/log>
        IndexOptions FancyIndexing FoldersFirst
        Options MultiViews Indexes
        AllowOverride None
        Order allow,deny
        Allow from all
        AuthType Basic
        AuthName "Authentication Required"
        AuthUserFile "/etc/.htpasswd"
        Require valid-user
    </Directory>
```

Теперь по адресу `example.com` доступна ваша папка.
