---
layout: post
title:  "Kubernetes"
date:   2019-11-18 13:37:00 +0300
slug: kubernetes
categories: ru K8S
locale: ru_RU
lang: ru
summary: "Заметки по Kubernetes"
---

### Тестовый пример

Можно использовать для тестов

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: nginx
  labels:
   app: nginx
spec:
 replicas: 3
 selector:
   matchLabels:
    app: nginx
 template:
   metadata:
    labels:
      app: nginx
   spec:
    containers:
     - name: nginx
       image: nginx:stable
       ports:
        - containerPort: 80
---
apiVersion: v1
kind: Service
metadata:
 name: nginx
spec:
 selector: 
  app: nginx
 ports:
  - protocol: TCP
    port: 80
    targetPort: 80
---
apiVersion: networking.k8s.io/v1beta1
kind: Ingress
metadata:
 name: nginx
 annotations:
    nginx.ingress.kubernetes.io/rewrite-target: /
spec:
  rules:
  - host: dev.example.com
    http:
       paths:
       - path: /
         backend:
           serviceName: nginx
           servicePort: 80
```

### Мониторинга

#### Установка

Для мониторинга `kubernetes` используется связка `prometheus+grafana+alertmanager`. Для это есть оператор [stable/prometheus-operator.](https://github.com/helm/charts/tree/master/stable/prometheus-operator){:rel="nofollow" target="_blank"}

Прежде, чем ставить сам оператор, надо создать свой `values.yaml`. `commonLabels` используется для назначения лейбла всем ресурсам, которые будут задеплоены. В последующем, этот лейбл надо будет использовать в `ServiceMonitor`, `PrometheusRule`, чтобы оператор мог их найти и прочесть.

```yaml
---
nameOverride: "prometheus"
commonLabels:
  monitoring: prometheus

alertmanager:
  enabled: true
  ingress:
    enabled: true
    labels: {}
    hosts:
      - alertmanager.example.com
    path: /
    tls: []
  alertmanagerSpec:
    storage:
     volumeClaimTemplate:
      spec:
        storageClassName: local-disks
        accessModes: ["ReadWriteOnce"]
        resources:
          requests:
            storage: 4Gi

grafana:
  enabled: true
  defaultDashboardsEnabled: true
  adminPassword: myadmin
  persistence:
    type: statefulset
    enabled: true
    accessModes:
      - ReadWriteOnce
    size: 9Gi
    finalizers:
      - kubernetes.io/pvc-protection
  ingress:
    enabled: true
    labels: {}
    hosts:
      - grafana.example.com
    path: /
    tls: []

prometheus:
  enabled: true
  ingress:
    enabled: true
    annotations: {}
    labels: {}
    hosts:
      - prometheus.example.com
    path: /
    tls: []
  prometheusSpec:
    replicas: 1
    serviceMonitorSelector:
      matchLabels:
        monitoring: prometheus
    ruleSelector:
      matchLabels:
        monitoring: prometheus
    podMonitorSelector:
      matchLabels:
        monitoring: prometheus
    storageSpec:
     volumeClaimTemplate:
       spec:
         storageClassName: local-disks
         accessModes: ["ReadWriteOnce"]
         resources:
           requests:
             storage: 9Gi
```
Теперь установим оператор в неймспейс `monitoring`:
```bash
$ kubectl create ns monitoring
$ helm upgrade -i mon stable/prometheus-operator --namespace monitoring -f values.yaml
```
#### How to monitor etcd with ssl

Сначала создаем секрет для прометеуса:
```bash
D="$(mktemp -d)"
cp /etc/kubernetes/pki/etcd/{ca.crt,healthcheck-client.{crt,key}} $D
kubectl create ns metrics
kubectl -n metrics create secret generic etcd-client --from-file="$D"
rm -fr "$D"
```

Затем, в `values.yml` в разделе `prometheus` добавляемЖ
```yaml
  secrets:
      - etcd-certificates
```

#### Kube-proxy target

Для того, чтобы прометеус мог собирать метрики с `kube-proxy` необходимо перебайндить адрес для них. По умолчанию, kube-proxy настроен на `127.0.0.1`. Чтобы это изменить, необходимо изменить его конфиг файл, который находится в ConfigMap `kube-proxy`:
```bash
$ kubectl -n kube-system edit cm kube-proxy
```
```yaml
...
metricsBindAddress: 0.0.0.0:10249
...
```
