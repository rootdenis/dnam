---
layout: post
title:  "Windows server"
date:   2019-11-11 15:38:00 +0300
slug: windows-server
categories: ru windows
locale: ru_RU
lang: ru
summary: "Заметки по MS Windows"
---

## DHCP

### Миграция DHCP сервера с Windows 2008 на Windows Server 2012

На новом сервере с Windows Server 2012  установим роль DHCP с помощью команд Powershell:
```azurepowershell
Import-Module ServerManager
Add-WindowsFeature -IncludeManagementTools dhcp
```

Следующий шаг – авторизация нового DHCP сервера в Active Directory. Эту операцию также выполним с помощью Powershell :
```azurepowershell
Add-DhcpServerInDC -DnsName srv-dhcp2012.winitpro.ru -IPAddress 192.168.10.22
```

Для экспорта настроек DHCP сервера, всех зон и резерваций с сервера под управление Windows Server 2008/R2, в консоли powershell на новом сервере (srv-dhcp2012) выполните следующую команду:
 
```azurepowershell
Export-DhcpServer -ComputerName srv-dhcp2008.winitpro.ru -File C:\DHCP\w2008dhcpconfig.xml -verbose
```

Конфигурация старого сервера  экспортируется в XML формате в файл w2008dhcpconfig.xml. Отключите службу DHCP на сервере srv-dhcp2008:

```azurepowershell
Stop-Service DHCPserver
```

Затем запускаем команду импорта:
```azurepowershell
Import-Dhcpserver –ComputerName srv-dhcp2012.winitpro.ru  -File C:\DHCP\w2008dhcpconfig.xml -BackupPath C:\ DHCP\backup\ -verbose
```

Если нужно импортировать только часть dhcp-зон, команда импорта будет выглядеть так:
```azurepowershell
Import-Dhcpserver –ComputerName srv-dhcp2012.winitpro.ru  -File C:\DHCP\w2008dhcpconfig.xml -BackupPath C:\ DHCP\backup\ -verbose –ScopeId 10.1.1.0, 10.1.2.0
```

## Active directory

### Работа с атрибутами
Из всех атрибутов объектов учетных записей пользователей, существуют всего шесть обязательных атрибутов:

- `cn` - Атрибут, который используется для отображения свойств имени. Это поле должно быть уникальным во всем домене, и заполняется автоматически;
- `instanceType` - Текущий атрибут указывает экземпляр объекта пользователя, который будет использоваться в качестве шаблона для нового объекта пользователя;
- `objectCategory` - Данный атрибут определяет категорию схемы Active Directory. Например, CN=Person,CN=Schema,CN=Configuration,DC=BIOPHARMACEUTIQUE,DC=COM;
- `objectClass` - Этот атрибут определяет класс объекта;
- `objectSid` - Определяет идентификатор безопасности объекта, который назначается автоматически;
- `sAMAccoountName` - Задает имя учетной записи SAM пользователя. Максимальная длина описания – 256 знаков. Конфигурируется данный атрибут непосредственно на основе данных, обеспечиваемых при создании учетной записи пользователя.

#### Изменение атрибутов на вкладках свойств учетной записи пользователя

##### Вкладка «Общие»

Данная вкладка содержит свойство имени, которое настраивается при создании объекта пользователя, а также его основное описание и контактные данные. На данной вкладке вы моете отконфигурировать девять атрибутов объекта пользователя, а именно: 
- `givenName` - имя пользователя
- `sn` фамилию
- `initials` инициалы
- `displayName` выводимое имя
- `description` описание объекта
- `physicalDeliveryOfficeName` номер комнаты
- `telephoneNumber` контактный номер телефона
- `mail` адрес электронной почты
- `wWWHomePage` адрес веб-сайта пользователя
- `otherTelephone` телефонный номер
- `url`  дополнительный веб-сайт. Если вы добавите более двух номеров телефона или веб-сайтов, они будут записываться в последние атрибуты через точку с запятой;

![adattr-01](/images/active-directory/adattr-01.jpg "adattr-01")

##### Вкладка «Адрес»

- `streetAddress` улицу, на которой живет ваш сотрудник
- `postOfficeBox` номер дома, который можно указать в текстовом поле «Почтовый ящик»
- `l` город, в котором живет ваш пользователь
- `st` Помимо этого вы можете указать область или край
- `postalCode` почтовый индекс
- страна (за этот параметр отвечают три атрибута, а именно `c` – буквенное сокращение страны, `co` – название страны, `countryCode` – код страны)

![adattr-02](/images/active-directory/adattr-02.jpg "adattr-02")

##### Вкладка «Учетная запись»

- `userPrincipalName` имя входа пользователя
- `sAMAccountName` имя входа пользователя пред-Windows 2000
- `logonHours` время входа пользователя (определяется атрибутом  в шестнадцатеричном формате).
- `userWorkstations` Также вы можете указать компьютеры, на которые пользователь может выполнять вход
- `accountExpires` Также вы можете указать срок действия учетной записи

Если учетная запись пользователя заблокирована, то вы можете ее разблокировать, установив соответствующий флажок. 

Помимо этого, вы можете добавить следующие параметры учетной записи: требовать смены пароля при следующем входе в систему, запрещение смены пароля пользователем установка неограниченного срока действия пароля и прочее. 

![adattr-03](/images/active-directory/adattr-03.jpg "adattr-03")

##### Вкладка «Профиль»

- `profilePath` путь к профилю пользователя
- `scripPath` сценарий входа
- `homeDirectory` домашняя папка
- `homeDrive` сетевой диск

![adattr-04](/images/active-directory/adattr-04.jpg "adattr-04")

##### Вкладка «Телефоны»

- `homePhone` домашний телефон
- `pager` номер пейджера
- `mobile` номер мобильного телефона
- `facsimileTelephoneNumber` номер факса
- `ipPhone` номер IP-телефона
- `info` текстовое поле «Заметки», которое используется для занесения дополнительной информации. 

Также вы можете добавить дополнительные номера телефонов (например, за дополнительный номер мобильного телефона отвечает атрибут `otherMobile`, за дополнительный номер пейджера – `otherPager`, за дополнительные номера домашнего телефона, факса и IP-телефона, соответственно, `otherHomePhone`, `otherFacsimileTelephoneNuber` и `otherIpPhone`);

![adattr-05](/images/active-directory/adattr-05.jpg "adattr-05")

##### Вкладка «Организация»

- `title` должность данного сотрудника
- `department` название отдела
- `company` наименование организации
- `manager`непосредственный начальника для текущего пользователя


Также просмотреть всех прямых подчиненных в поле `«Прямые подчиненные»`

![adattr-06](/images/active-directory/adattr-06.jpg "adattr-06")

### Ссылки

- [Атрибуты учеток](http://www.oszone.net/14239/attributes){:rel="nofollow" target="_blank"}
- [Миграция DHCP](http://winitpro.ru/index.php/2013/02/05/migraciya-dhcp-servera-na-windows-server-2012/){:rel="nofollow" target="_blank"}
