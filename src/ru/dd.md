---
layout: post
title:  "dd"
date:   2020-01-09 11:45:00 +0300
slug: dd
categories: ru fs
locale: ru_RU
lang: ru
summary: "Использование утилиты dd"
---

### dd to remote host

run from remote host:
```bash
$ dd if=/dev/sda | gzip -1 - | ssh user@local dd of=image.gz
```

run from localhost:
```bash
$ ssh user@remote "dd if=/dev/sda | gzip -1 -" | dd of=image.gz
```
