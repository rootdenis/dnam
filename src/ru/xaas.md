---
layout: post
title:  "IaaS, PaaS или SaaS"
date:   2017-08-11 13:24:46 +0300
slug: xaas
categories: ru IT
locale: ru_RU
lang: ru
summary: "Какие бывают сервисы"
---

<img src="/images/xaas/xaas.png" style="width: 200.0px;">

###### Разновидности

Существует несколько разновидностей услуги (сервиса)

- IaaS (Infrastructure as a Service) - инфраструктура как услуга
- PaaS (Platform as a Service) - платформа как услуга
- SaaS (Software as a Service) - программное обеспечение как услуга

###### Сравнения

Если сравнивать очень примитивно то всё это можно сравнить так:

- IaaS (Infrastructure as a Service) - это сдача в аренду железной структуры самый низкий уровень
- PaaS (Platform as a Service) - это сдача в аренду платформы например VPS/VDS
- SaaS (Software as a Service) - это сдача в аренду программы, это самый верхний уровень например гугл блоги это SaaS
