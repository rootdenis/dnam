---
layout: post
title:  "SetCap"
date:   2019-11-22 09:11:00 +0300
slug: devport
categories: ru dev
locale: ru_RU
lang: ru
summary: "cat: /dev/port: Operation not permitted"
---

Если ты не `root` то писать и читать из `/dev/port` просто так не получается. Что бы получилось это сделать надо немного уличной магии.

### Пример с программой `cat`
```bash
cp /usr/bin/cat .
sudo setcap cap_sys_rawio=ep ./cat
```
После чего можо запустить
```bash
./cat /dev/port
```
Доступ програма получает

### Пример с программой `wine`
```bash
sudo setcap cap_net_raw+eip /usr/bin/wineserver
```

### Принцип работы
Устанавливается расширенный атрибут файла, позволяет выполнять себя от имеи root
