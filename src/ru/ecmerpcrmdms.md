---
layout: post
title:  "Системы класса ECM/ERP/CRM/DMS"
date:   2016-11-13 09:06:00 +0300
slug: ecmerpcrmdms
categories: ru IT
locale: ru_RU
lang: ru
summary: "Список систем класса ECM/ERP/CRM и применение их как аналогов 1с, а в большей части функционал выходиз за рамки замнителей 1с"
---

После случая с "слёт лицензий 1с" для себя определил список продуктов решающих такие же задачи:

* [Liferay](https://ru.wikipedia.org/wiki/Liferay){:rel="nofollow" target="_blank"} (CMS) программный продукт, представляющий собой корпоративный портал.
* [Alfresco](https://ru.wikipedia.org/wiki/Alfresco_(ECM-%D1%81%D0%B8%D1%81%D1%82%D0%B5%D0%BC%D0%B0)){:rel="nofollow" target="_blank"} (ECM) он же (СЭД) Cистема Электронного Документооборота
* [Odoo](https://ru.wikipedia.org/wiki/Odoo){:rel="nofollow" target="_blank"} (ERP) и (CRM) система, разрабатываемая бельгийской компанией Odoo S. A. (бывшая Tiny ERP, OpenERP)
* [OpenCms](https://ru.wikipedia.org/wiki/OpenCms){:rel="nofollow" target="_blank"} (CMS) коммерческого уровня с открытым исходным кодом и свободной лицензией.
* [Nuxeo](https://ru.wikipedia.org/wiki/Nuxeo){:rel="nofollow" target="_blank"}
* [Jahia](https://ru.wikipedia.org/wiki/Jahia){:rel="nofollow" target="_blank"}
* [Activiti](http://activiti.org/){:rel="nofollow" target="_blank"} продукт позволяет описывать бизнес-процессы в нотации BPMN 2.0 и выполнять их.
* [Jaspersoft Business Intelligence](http://www.jaspersoft.com/){:rel="nofollow" target="_blank"} средство анализа данных, полученных из разных источников, формирования отчетов и графиков.
* [Mule](https://developer.mulesoft.com/){:rel="nofollow" target="_blank"} (ESB) легковесная интеграционная платформа.
* [LogicalDOC](http://www.logicaldoc.com/en.html){:rel="nofollow" target="_blank"} (DMS) предназначена для управления документами внутри компании.
* [Магнолия](https://www.magnolia-cms.com/){:rel="nofollow" target="_blank"} (CMS) система управления контентом.
* [Exadata](http://ru.wikipedia.org/wiki/Exadata/){:rel="nofollow" target="_blank"}
* [Compiere](http://ru.wikipedia.org/wiki/Compiere/){:rel="nofollow" target="_blank"}
* [ADempiere](http://ru.wikipedia.org/wiki/ADempiere/){:rel="nofollow" target="_blank"}
* [Oracle Collaboration Suite](http://www.oracle.com/technetwork/middleware/collaboration-suite/overview/index.html){:rel="nofollow" target="_blank"}
* [SAP](http://www.ibm.com/ru/solutions/sap/){:rel="nofollow" target="_blank"}
* [SAS](http://www.sas.com/offices/europe/russia/){:rel="nofollow" target="_blank"}
* [Opentaps](http://ru.wikipedia.org/wiki/Opentaps/){:rel="nofollow" target="_blank"}
* [на базе Oracle BPEL+UCM](http://www.fors.ru/portal/page?_pageid=113,243880&_dad=portal&_schema=PORTAL){:rel="nofollow" target="_blank"}
* [Корпорация Парус](http://ru.wikipedia.org/wiki/Парус_(программный_продукт)){:rel="nofollow" target="_blank"} [Парус](http://www.parus.com/){:rel="nofollow" target="_blank"}
* [Millennium BSA](http://www.m-g.ru/){:rel="nofollow" target="_blank"}
* [Дебет Плюс](http://www.debet.kiev.ua/){:rel="nofollow" target="_blank"}
* [Своя технология](http://ctex.ru/){:rel="nofollow" target="_blank"}
* [Высший Сорт](http://www.v-sort.ru/vsbuxgalteriya.php){:rel="nofollow" target="_blank"}
* [Победит](http://www.pobedit.com/){:rel="nofollow" target="_blank"}
* [Ананас](http://ananas.su/wiki/){:rel="nofollow" target="_blank"}
* [Афина](http://athenasoft.ru/){:rel="nofollow" target="_blank"}
* [Лексема](http://www.lexema.ru/){:rel="nofollow" target="_blank"}
* [New Solutions Group](http://www.nsgsoft.ru/){:rel="nofollow" target="_blank"}
* [Plazma ERP+CRM](http://plazma.sourceforge.net/frameaction.php?lng=ru&page=start){:rel="nofollow" target="_blank"}

**Скисок систем управления проектами, отслеживания ошибок и организации совместной работы групп людей**

* [Redmine](http://ru.wikipedia.org/wiki/Redmine){:rel="nofollow" target="_blank"}
* [Jira](https://ru.wikipedia.org/wiki/Jira){:rel="nofollow" target="_blank"}
* [Bitbucket](https://ru.wikipedia.org/wiki/Bitbucket){:rel="nofollow" target="_blank"}
* [GitHub](https://ru.wikipedia.org/wiki/GitHub){:rel="nofollow" target="_blank"}
* [YouTrack](https://ru.wikipedia.org/wiki/YouTrack){:rel="nofollow" target="_blank"}
* [Asana](https://ru.wikipedia.org/wiki/Asana){:rel="nofollow" target="_blank"}
* [GLPI](http://ru.wikipedia.org/wiki/GLPI){:rel="nofollow" target="_blank"}
* [Trac](http://ru.wikipedia.org/wiki/Trac){:rel="nofollow" target="_blank"}
* [Mantis](http://ru.wikipedia.org/wiki/Mantis_(программа)){:rel="nofollow" target="_blank"}
* [Bugzilla](http://ru.wikipedia.org/wiki/Bugzilla){:rel="nofollow" target="_blank"}
* [OpenProj](http://ru.wikipedia.org/wiki/OpenProj){:rel="nofollow" target="_blank"}
* [GanttProject](http://ru.wikipedia.org/wiki/GanttProject){:rel="nofollow" target="_blank"}
* [EGroupWare](http://ru.wikipedia.org/wiki/EGroupWare){:rel="nofollow" target="_blank"}
* [Zimbra](http://ru.wikipedia.org/wiki/Zimbra){:rel="nofollow" target="_blank"}
* [Citadel](http://ru.wikipedia.org/wiki/Citadel_(groupware)){:rel="nofollow" target="_blank"}
* [Bongo](http://ru.wikipedia.org/wiki/Bongo){:rel="nofollow" target="_blank"}
* [Feng Office Community Edition](http://ru.wikipedia.org/wiki/Feng_Office_Community_Edition){:rel="nofollow" target="_blank"}
* [BG ERP](https://bgerp.ru/){:rel="nofollow" target=blank}

Ссылки по теме:

* [CMF](https://ru.wikipedia.org/wiki/Content_Management_Framework){:rel="nofollow" target="_blank"} (фреймворк программной системы) для проектирования систем управления контентом.
* [CMS](https://ru.wikipedia.org/wiki/%D0%A1%D0%B8%D1%81%D1%82%D0%B5%D0%BC%D0%B0_%D1%83%D0%BF%D1%80%D0%B0%D0%B2%D0%BB%D0%B5%D0%BD%D0%B8%D1%8F_%D1%81%D0%BE%D0%B4%D0%B5%D1%80%D0%B6%D0%B8%D0%BC%D1%8B%D0%BC){:rel="nofollow" target="_blank"} система управления содержимым.
* [ECM](https://ru.wikipedia.org/wiki/%D0%A3%D0%BF%D1%80%D0%B0%D0%B2%D0%BB%D0%B5%D0%BD%D0%B8%D0%B5_%D0%BA%D0%BE%D1%80%D0%BF%D0%BE%D1%80%D0%B0%D1%82%D0%B8%D0%B2%D0%BD%D1%8B%D0%BC_%D0%BA%D0%BE%D0%BD%D1%82%D0%B5%D0%BD%D1%82%D0%BE%D0%BC){:rel="nofollow" target="_blank"} управление корпоративным контентом.
* [CRM](https://ru.wikipedia.org/wiki/%D0%A1%D0%B8%D1%81%D1%82%D0%B5%D0%BC%D0%B0_%D1%83%D0%BF%D1%80%D0%B0%D0%B2%D0%BB%D0%B5%D0%BD%D0%B8%D1%8F_%D0%B2%D0%B7%D0%B0%D0%B8%D0%BC%D0%BE%D0%BE%D1%82%D0%BD%D0%BE%D1%88%D0%B5%D0%BD%D0%B8%D1%8F%D0%BC%D0%B8_%D1%81_%D0%BA%D0%BB%D0%B8%D0%B5%D0%BD%D1%82%D0%B0%D0%BC%D0%B8){:rel="nofollow" target="_blank"} система управления взаимоотношениями с клиентами.
* [ESB](https://ru.wikipedia.org/wiki/%D0%A1%D0%B5%D1%80%D0%B2%D0%B8%D1%81%D0%BD%D0%B0%D1%8F_%D1%88%D0%B8%D0%BD%D0%B0_%D0%BF%D1%80%D0%B5%D0%B4%D0%BF%D1%80%D0%B8%D1%8F%D1%82%D0%B8%D1%8F){:rel="nofollow" target="_blank"} сервисная шина предприятия.
* [DMS](https://ru.wikipedia.org/wiki/%D0%A1%D0%B8%D1%81%D1%82%D0%B5%D0%BC%D0%B0_%D1%83%D0%BF%D1%80%D0%B0%D0%B2%D0%BB%D0%B5%D0%BD%D0%B8%D1%8F_%D0%B4%D0%BE%D0%BA%D1%83%D0%BC%D0%B5%D0%BD%D1%82%D0%B0%D0%BC%D0%B8){:rel="nofollow" target="_blank"} система управления документами.
* [ERP](https://ru.wikipedia.org/wiki/ERP){:rel="nofollow" target="_blank"} организационная стратегия интеграции производства и операций, управления трудовыми ресурсами, финансового менеджмента и управления активами, ориентированная на непрерывную балансировку и оптимизацию ресурсов предприятия посредством специализированного интегрированного пакета прикладного программного обеспечения, обеспечивающего общую модель данных и процессов для всех сфер деятельности.

[Список Java фреймворков CMSок и т.п.](https://dzone.com/articles/top-21-java-based-content){:rel="nofollow" target="_blank"}
