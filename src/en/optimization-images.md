---
layout: post
title: "Optimizing images for the web"
date: 2016-06-27 15:15:09 +0300
slug: optimization-images
categories: en web
locale: en_US
lang: en
summary: "Google checker (website speed and optimization) often swears at unoptimized images. Attempts to overcome the slowness of the content were unsuccessful, so I decided to automate via cron and the image optimizer."
---


###### First option

First you need to install jpegoptim on the server

```bash
sudo apt install optipng
```

Then set up automatic image optimization

```bash
#[min] (0-59)
#|   [hour] (0-23)
#|   |    [day of the month] (1-31)
#|   |    |    [month] (1-12)
#|   |    |    |    [days of the week] (0-6 with 0=Sun)
#|   |    |    |    |    [user](only for system cron)
#|   |    |    |    |    |    [command]
#|   |    |    |    |    |    |
0    3    *    *    *         /usr/bin/find /var/www/sites/default/files/ -iname '*.png' -print0 |xargs -0 optipng -o7
#EOF
```

###### Second option

```bash
sudo apt install pngcrush
```

Best compression

```bash
pngcrush -rem alla -rem text -reduce -brute in.png out.png
```

Automatic optimization

```bash
pngcrush -reduce -brute in.png out.png
```

Remove all color correction data from file

```bash
pngcrush -rem gAMA -rem cHRM -rem iCCP -rem sRGB in.png out.png
```

Remove auxiliary data

```bash
pngcrush -rem alla -rem text in.png out.png
```

Optimize all files in a loop

```bash
for file in *.png ; do pngcrush -reduce -brute -rem alla -rem gAMA -rem cHRM -rem iCCP -rem sRGB "$file" "${file%.png}-crushed.png" && mv "${file%.png}-crushed.png" "$file" ; done
```

First you need to install jpegoptim on the server

```bash
sudo apt install jpegoptim
```

Then set up automatic image optimization

```bash
#[min] (0-59)
#|   [hour] (0-23)
#|   |    [day of the month] (1-31)
#|   |    |    [month] (1-12)
#|   |    |    |    [days of the week] (0-6 with 0=Sun)
#|   |    |    |    |    [user](only for system cron)
#|   |    |    |    |    |    [command]
#|   |    |    |    |    |    |
0    3    *    *    *         /usr/local/bin/jpegoptim --strip-all /usr/local/www/sites/default/files/*.jpg
#EOF
```
