---
layout: post
title:  "What do you know about DNS"
date:   2021-03-05 15:21:10 +0300
slug: dns
categories: en dns rfc
locale: en_US
lang: en
summary: "What do you know about DNS"
---

##### Do you know how DNS works?

The other day it turned out that one stupid colleague does not understand how DNS works at all. Moreover, he doesn't want to understand.
He used to apply the Bind package when he was a sysadmin. In fairness, he's easier to pity.
At first I tried to argue and prove something, but then I realized that "A nod’s as good as a wink to a blind horse".

[Bind][BIND]{:rel="nofollow" target="blank"} is both authoritarian and recursive DNS server.

Unfortunately, therefore, many people do not understand that there are two completely different tasks:
- One for zone maintenance.
- The second is recursive name resolution.

If you want to understand the difference, take a look at the [NSD][NSD]{:rel="nofollow" target="blank"} and [Unbound][UNBOUND]{:rel="nofollow" target="blank"} servers. Or the [PowerDNS][POWERDNS]{:rel="nofollow" target="blank"} server it also consists of two components

About TCP and UDP:
```text
Most DNS RFC1034 transactions take place over UDP RFC0768.
TCP RFC0793 is always used for zone transfers and is often used for messages whose sizes exceed the DNS protocol's original 512-byte limit.
```

About authoritative server:
```text
Authoritative server implementations MUST support TCP so that they
do not limit the size of responses to what fits in a single UDP
packet.
```

About recursive server:
```text
Recursive server (or forwarder) implementations MUST support TCP
so that they do not prevent large responses from a TCP-capable
server from reaching its TCP-capable clients.
```

About stub resolver (for small installation):
```text
Stub resolver implementations (e.g., an operating system's DNS
resolution library) MUST support TCP since to do otherwise would
limit their interoperability with their own clients and with
upstream servers. 

Stub resolver implementations MAY omit support for TCP when
specifically designed for deployment in restricted environments where
truncation can never occur or where truncated DNS responses are
acceptable.
```


About protocols:
```text
A resolver SHOULD send a UDP
query first, but MAY elect to send a TCP query instead if it has good
reason to expect the response would be truncated if it were sent over
UDP (with or without EDNS0) or for other operational reasons, in
particular, if it already has an open TCP connection to the server.
```

### Links
- [RFC5966][RFC5966]{:rel="nofollow" target="_blank"}
- [RFC1034][RFC1034]{:rel="nofollow" target="_blank"}
- [RFC0768][RFC0768]{:rel="nofollow" target="_blank"}
- [RFC0793][RFC0793]{:rel="nofollow" target="_blank"}

[RFC5966]: https://tools.ietf.org/html/rfc5966
[RFC1034]: https://tools.ietf.org/html/rfc1034
[RFC0768]: https://tools.ietf.org/html/rfc0768
[RFC0793]: https://tools.ietf.org/html/rfc0793
[UNBOUND]: https://www.nlnetlabs.nl/projects/unbound/about/
[NSD]: https://www.nlnetlabs.nl/projects/nsd/about/
[POWERDNS]: https://www.powerdns.com/
[BIND]: https://www.isc.org/bind/
