---
layout: post
title:  "How to drop broken replication slot. PostgreSQL"
date:   2020-12-21 08:18:10 +0300
slug: pgdropreplicationslot
categories: en SQL Postgresql
locale: en_US
lang: en
summary: "How to drop broken replication slot. Postgresql"
---

If you have a broken postgres slave. WAL files will be created until the disk space runs out.

We need to find out which slot can be deleted

```sql
postgres=# SELECT * FROM pg_replication_slots;
 slot_name | plugin | slot_type | datoid | database | temporary | active | active_pid | xmin | catalog_xmin | restart_lsn  | confirmed_flush_lsn 
-----------+--------+-----------+--------+----------+-----------+--------+------------+------+--------------+--------------+---------------------
 pgslave2  |        | physical  |        |          | f         | t      |      25762 |      |              | 89D/B02055D8 | 
 pgslave1  |        | physical  |        |          | f         | t      |            |      |              | 74D/C12064B1 | 
(2 rows)
```

Delete the unnecessary one

```sql
postgres=# select pg_drop_replication_slot('pgslave1');
```

Check it out

```sql
postgres=# SELECT * FROM pg_replication_slots;
 slot_name | plugin | slot_type | datoid | database | temporary | active | active_pid | xmin | catalog_xmin | restart_lsn  | confirmed_flush_lsn 
-----------+--------+-----------+--------+----------+-----------+--------+------------+------+--------------+--------------+---------------------
 pgslave2  |        | physical  |        |          | f         | t      |      25762 |      |              | 89D/B02055D8 | 
(1 row)
```

Find the last WAL

```sql
postgres=# SELECT * FROM pg_stat_archiver  \gx
-[ RECORD 1 ]------+------------------------------
archived_count     | 14525
last_archived_wal  | 000000010000089D000000B6
last_archived_time | 2020-12-21 08:54:25.563978+03
failed_count       | 0
last_failed_wal    |
last_failed_time   |
stats_reset        | 2020-01-17 01:14:35.400937+03
```

And clear the unnecessary

```bash
pg_archivecleanup -d /var/lib/postgresql/12/main/pg_wal 000000010000089D000000B6
```
