---
layout: post
title:  "How to disable user"
date:   2021-10-12 08:22:10 +0300
slug: disable-user
categories: en linux usermod
locale: en_US
lang: en
summary: "How to disable user"
---


##### Expire Account

How you can disable user

```bash
usermod --expiredate 1 vpupkin
```

or

```bash
usermod -L -e 1 vpupkin
```

How youcan enable the user

```bash
sudo usermod --expiredate "" vpupkin
```

or

```bash
sudo usermod -U -e "" vpupkin
```

##### Lock a Password

But this doesn't work for ssh with ssh-key

Lock password for user

```bash
passwd -l vpupkin
```

Unlock password for user

```bash
passwd -u vpupkin
```

##### Expire a Password

```bash
passwd -e YYYY-MM-DD vpupkin
```
