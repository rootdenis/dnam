---
layout: post
title:  "Wi-Fi and ASUS TUF Gaming FX505DY"
date:   2020-10-26 12:31:10 +0300
slug: rtl8821ce
categories: en debian ubuntu rtl8821ce
locale: en_US
lang: en
summary: "How to Install Wi-Fi Driver for ASUS TUF Gaming FX505DY"
---

##### How to Install Wi-Fi Driver for ASUS TUF Gaming FX505DY. [ru](/ru/rtl8821ceru)

If you're a happy owner of an asus laptop, this is  for you. And if you're a lucky owner of ubuntu - this is definitely for you.

```bash

sudo apt-get install --reinstall git dkms build-essential linux-headers-$(uname -r)
git clone https://github.com/tomaspinho/rtl8821ce
cd rtl8821ce
chmod +x dkms-install.sh
chmod +x dkms-remove.sh
sudo ./dkms-install.sh

```


### Links
- [ASUS TUF Gaming FX505DY][FX505DY]{:rel="nofollow" target="_blank"}

[FX505DY]: https://www.asus.com/uk/Laptops/ASUS-TUF-Gaming-FX505DY/
