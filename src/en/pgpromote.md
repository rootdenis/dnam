---
layout: post
title:  "How to promote a slave to the master. Postgresql"
date:   2023-04-06 16:08:10 +0200
slug: pgpromote
categories: en SQL Postgresql
locale: en_US
lang: en
summary: "How to promote a slave to the master. Postgresql"
---

## To switch slave to the master mode:

### Find out the path to the directory

```sql
postgres=# show data_directory;
     data_directory
------------------------
 /storage/postgresql/db
(1 row)
```

### Add this path to the environment variable

```bash
export PGDATA="/storage/postgresql/db"
```

### And promote the base to the master mode

```bash
/usr/lib/postgresql/13/bin/pg_ctl promote
```

