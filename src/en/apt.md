---
layout: post
title:  "APT IPv6 or IPv4 on Ubuntu/Debian"
date:   2020-10-26 13:18:10 +0300
slug: apt4and6
categories: en OS
locale: en_US
lang: en
summary: "Force APT to IPv6 or IPv4 on Ubuntu/Debian"
---

For force IPv6 or IPv4 you hav to use:

```
apt -o Acquire::ForceIPv6=true update
```

or

```
apt -o Acquire::ForceIPv4=true update
```

Ceate the file `99force-ipv6` in `/etc/apt/apt.conf.d/` if you want it to be persistent.

```
#/etc/apt/apt.conf.d/99force-ipv6
Acquire::ForceIPv6 "true";
```
