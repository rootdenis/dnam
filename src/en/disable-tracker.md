---
layout: post
title:  "How to completely disable tracker"
date:   2021-10-12 08:22:10 +0300
slug: disable-tracker
categories: en ubuntu ssd
locale: en_US
lang: en
summary: "How to completely disable tracker"
---

[Tracker][TRACKER]{:rel="nofollow" target="blank"} is a filesystem indexer, metadata storage system and search tool.

##### For disable tracker2


```
systemctl --user mask tracker-store.service tracker-miner-fs.service tracker-miner-rss.service tracker-extract.service tracker-miner-apps.service tracker-writeback.service
tracker reset --hard
```

##### For disable Tracker3


```
systemctl --user mask tracker-extract-3.service tracker-miner-fs-3.service tracker-miner-rss-3.service tracker-writeback-3.service tracker-xdg-portal-3.service tracker-miner-fs-control-3.service
tracker3 reset -s -r
```

##### For enable tracker2


```
systemctl --user unmask tracker-store.service tracker-miner-fs.service tracker-miner-rss.service tracker-extract.service tracker-miner-apps.service tracker-writeback.service
```

##### For enable Tracker3


```
systemctl --user unmask tracker-extract-3.service tracker-miner-fs-3.service tracker-miner-rss-3.service tracker-writeback-3.service tracker-xdg-portal-3.service tracker-miner-fs-control-3.service
```

### Links
- [Tracker][TRACKER]{:rel="nofollow" target="_blank"}
- [Tracker project][TRACKER-PROJECT]{:rel="nofollow" target="_blank"}

[TRACKER]: https://wiki.gnome.org/Projects/Tracker
[TRACKER-PROJECT]: https://gnome.pages.gitlab.gnome.org/tracker
