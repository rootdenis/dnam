---
layout: post
title:  "How to use rubbish gulpjs"
date:   2020-11-20 14:18:10 +0300
slug: dockergulpjs
categories: en docker js
locale: en_US
lang: en
summary: "How to use rabbish gulpjs in to Docker"
---


One programmer decided that it would be easier to minify the project with gulpjs. But as always, it turned out to be more difficult. Because this garbage "gulpjs" doesn't work if installed globally. That's why I made this crutch.

First of all I made the script to solve this problem:

```bash
#!/bin/sh

USERID=$(stat -c '%u' .)

cp -r /root/node_modules . && chown ${USERID}:${USERID} -R ./node_modules
# vim: set filetype=bash et sw=2 ts=2:
```

And packed it into docker

Dockerfile to solve this problem:

```bash
FROM node:alpine

LABEL \
  org.label-schema.docker.cmd="docker run registry.example.org/gulp:latest " \
  org.label-schema.description="html css ja" \
  org.label-schema.name="node" \
  org.label-schema.schema-version=20201120 \
  org.label-schema.vcs-url="https://gitlab.example.org/docker/-/tree/master/gulp" \
  org.label-schema.vendor="devops-team" \
  org.label-schema.version=20201120

COPY cpnode.sh /cpnode.sh

run set -x \
  && npm -g install gulp-cli \
    gulp \
  && cd /root \
  && npm --save-dev install gulp-cli \
    vinyl-sourcemaps-apply \
    gulp-javascript-obfuscator \
    gulp-minify \
    gulp-concat \
    gulp-htmlmin \
    gulp-minify-inline \
    gulp

CMD ["/cpnode.sh"]
# vim: set filetype=dockerfile et sw=2 ts=2:
```

