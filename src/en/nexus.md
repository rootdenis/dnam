---
layout: post
title:  "How to make a Docker Proxy with Sonatype Nexus"
date:   2021-03-10 15:24:10 +0300
slug: 
categories: en nexus docker proxy sonatype
locale: en_US
lang: en
summary: "Configuring a docker Proxy server with Sonatype Nexus"
---

##### Configuring a docker Proxy server with Sonatype Nexus
<img src="/images/nexus/nexusrepo.png" style="width: 150.0px;" alt="Nexus" >

First of all I created a repository with the "proxy" type.

For Quay.io

<img src="/images/nexus/nexus-repo-quayio.png" style="width: 800.0px;" alt="Nexus repo quayio" title="Nexus repo quayio" >

For Docker Hub

<img src="/images/nexus/nexus-repo-dockerhub.png" style="width: 800.0px;" alt="Nexus repo dockerhub" title="Nexus repo dockerhub" >

For JFrog

<img src="/images/nexus/nexus-repo-jfrog.png" style="width: 800.0px;" alt="Nexus repo jfrog" title="Nexus repo jfrog" >

After that I created a repository with the "group" type and port 5000.

<img src="/images/nexus/nexus-docker-group-01.png" style="width: 800.0px;" alt="Nexus docker group 01" title="Nexus docker group 01" >

And added the repositories to the group.

<img src="/images/nexus/nexus-docker-group-02.png" style="width: 800.0px;" alt="Nexus docker group 02" title="Nexus docker group 02" >

The list of all repositories looks something like this

<img src="/images/nexus/nexus-repos.png" style="width: 800.0px;" alt="Nexus repos" title="Nexus repos" >

After that, I set up realm to anonymous "docker pull".

<img src="/images/nexus/nexus-realm.png" style="width: 1000.0px;" alt="Nexus realm" title="Nexus realm">

And "Anonymous Access"

<img src="/images/nexus/nexus-realm-anonymous.png" style="width: 800.0px;" alt="Nexus realm anonymous" title="Nexus realm anonymous" >

Need to add domains as secure. If no certificates are planned.

```text
/etc/docker/daemon.json
```

I will explain a little later why a domain without a port

{% highlight json %}
{
"insecure-registries": [
  "mydomain.loc",
  "mydomain.loc:5000"
],
"disable-legacy-registry": true
}
```

It is possible not to specify a unsecure domain, but I will have to put the root self-signed certificate everywhere.

I have to restart the daemon after setting `sudo systemctl restart docker`.

After that I pulled the images:
- `nginx` - from Docker Hub
- `coreos/flannel` - from RedHad registry
- `jfrog/artifactory-jcr` - from JFrog registry

```bash
docker pull mydomain.loc:5000/nginx
docker pull mydomain.loc:5000/coreos/flannel:v0.13.1-rc2
docker pull mydomain.loc:5000/jfrog/artifactory-jcr
```

```bash
docker images
mydomain.loc:5000/jfrog/artifactory-jcr   latest        da6f84d1ad96   7 days ago    806MB
mydomain.loc:5000/nginx                   latest        35c43ace9216   2 weeks ago   133MB
mydomain.loc:5000/coreos/flannel          v0.13.1-rc2   dee1cac4dd20   5 weeks ago   64.3MB
```

So far, so good, but the port is upsetting me. I configured the nginx proxy to get rid of the port

{% highlight conf %}
upstream registry_server { server 127.0.0.1:5000 fail_timeout=0; }

server {
  listen 80;
  server_name mydomain.loc
  server_tokens off;
  reset_timedout_connection on;
  charset UTF-8;
  client_max_body_size 0;
  location / {
    proxy_read_timeout 900;
    proxy_set_header Host $host;
    proxy_set_header X-Real-IP $remote_addr;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_pass http://registry_server/;
  }
}
```

It is always darkest before the dawn.

```bash
docker pull mydomain.loc/nginx
docker pull mydomain.loc/coreos/flannel:v0.13.1-rc2
docker pull mydomain.loc/jfrog/artifactory-jcr
```

```bash
docker images
mydomain.loc/jfrog/artifactory-jcr   latest        da6f84d1ad96   7 days ago    806MB
mydomain.loc/nginx                   latest        35c43ace9216   2 weeks ago   133MB
mydomain.loc/coreos/flannel          v0.13.1-rc2   dee1cac4dd20   5 weeks ago   64.3MB
```

### Links
- [Git docker-nexus][DOCKER-NEXUS]{:rel="nofollow" target="_blank"}
- [Nexus][NEXUS]{:rel="nofollow" target="_blank"}
- [Proxy Repository for Docker][NEXUS_REPOSITORY_DOCKER]{:rel="nofollow" target="_blank"}

[DOCKER-NEXUS]: https://gitlab.com/dtulyakov/docker-nexus
[NEXUS]: https://www.sonatype.com/nexus/repository-oss
[NEXUS_REPOSITORY_DOCKER]: https://help.sonatype.com/repomanager3/formats/docker-registry/proxy-repository-for-docker
