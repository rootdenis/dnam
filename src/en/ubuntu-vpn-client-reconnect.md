---
layout: post
title:  "VPN client reconnect"
date:   2020-07-13 12:18:00 +0300
slug: ubuntu-vpn-reconnect
categories: en VPN net
locale: en_US
lang: en
summary: "Ubuntu VPN client reconnect"
---


### Vhat is it

A virtual private network (VPN) extends a private network across a public network and enables users to send and receive data across shared or public networks as if their computing devices were directly connected to the private network. Applications running across a VPN may therefore benefit from the functionality, security, and management of the private network. Encryption is a common, although not an inherent, part of a VPN connection.

### Requirements

Sometimes the connection is lost. Gnome plugin does not reconnect after loss of connection. But this is wrong way. NetworkManager have got a `vpn.persistent` - `no` by default.

### How to fix this problem

```bash
sudo nmcli connection modify myVPNConnectionName vpn.persistent yes
```

### Links

- [nmcli](https://developer.gnome.org/NetworkManager/stable/nmcli.html){:rel=nofollow target=_blank}
