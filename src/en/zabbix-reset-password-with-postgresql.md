---
layout:     post
title:      "How to reset Zabbix password with postgresql"
date:       2020-12-07 08:18:10 +0300
slug:       zabbix-reset-password-with-postgresql
categories: en docker zabbix postgresql
locale:     en_US
lang:       en
summary:    "How to reset Zabbix password with postgresql"
---


### If unfortunately the password for Zabbix has been lost. How can you change this

You have to login to database CLI and connect to zabbix database.

```bash
sudo -u postgres psql
\c zabbix
```

Or you can do like this

```bash
sudo -u postgres psql zabbix
```

You have to change some user's password to a new password

```bash
update users set passwd=md5('superduperpassword') where alias='Admin';
```

If you want to be sure that everything worked out, you need to do it (the hash must be different)

```bash
select * from users;
```

No need to restart zabbix
